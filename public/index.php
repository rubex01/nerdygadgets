<?php

namespace {

    include __DIR__ . '/../Framework/autoload.php';
    include __DIR__ . '/../Framework/bootstrap.php';
    include __DIR__ . '/../vendor/autoload.php';

    use Framework\Auth\Auth;
    use Framework\Routing\Route;
    use App\Config;
    use Framework\CustomPackages\AdminAuth\AdminAuth;

    include __DIR__ . '/../Web/DBConnections.php';

    if (Config::$enabledFunctions['authorization']) {
        $Authorization = new Auth();
    }

    if (Config::$enabledFunctions['adminAuthorization']) {
        $adminAuthorization = new AdminAuth();
    }

    include __DIR__ . '/../Web/Routes.php';

    Route::run();

}

