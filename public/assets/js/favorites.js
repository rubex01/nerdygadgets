document.addEventListener("DOMContentLoaded", () => {

    $('#favoritesDropdownList').on('show.bs.dropdown', () => {
        updateFavorites()
    })

    const updateFavorites = () => {
        if (document.querySelector('.login-status').value == 'true') {
            getFavorites().then(favorites => {
                let table = document.querySelector('#favoritesTable')
                    table.innerHTML = '';
                    if (favorites.length == 0) {
                        let newRow = table.insertRow(0)
                        let cell1 = newRow.insertCell(0)
                        cell1.innerHTML = '<p class="navigation-list-dropdown-favorites-list-table-nofavorites">Geen producten...</p>'
                    }
                    favorites.forEach(favoriteItem => {
                        let newRow = table.insertRow(0)
                        let cell1 = newRow.insertCell(0)
                        let cell2 = newRow.insertCell(1)
                        cell1.innerHTML = '<img src="/assets/images/products/StockItemIMG/' + favoriteItem.ImagePath + '" class="navigation-list-dropdown-favorites-list-table-img" />'
                        cell2.innerHTML = '<a href="/product/' + favoriteItem.StockItemID + '" class="navigation-list-dropdown-favorites-list-table-link"><p class="navigation-list-dropdown-favorites-list-table-text">' + favoriteItem.StockItemName + '</p></a>'
                    })
                }
            )
        }
    }

    async function getFavorites() {
        const response = await fetch('/api/favorites', {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'same-origin'
        });
        return response.json();
    }

    updateFavorites()

});