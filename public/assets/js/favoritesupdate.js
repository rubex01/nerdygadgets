document.addEventListener("DOMContentLoaded", () => {

    const element = document.querySelectorAll('.shoppingcart-trigger')

    element.forEach(function(el){
        el.addEventListener('click', (e) => {
            let productId = e.target.dataset.id
            const csrf = document.querySelector('.csrf').value

            addProductToShoppingCart({
                'StockItemID': productId,
                'method': '+',
                'amount': '+1',
                'CSRFtoken': csrf
            }).then(() => {
                $('#shoppingcartDropdown').dropdown('show')
            })
        });
    });

    async function addProductToShoppingCart(data) {
        const response = await fetch('/api/shoppingcart', {
            method: 'PUT',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response;
    }

})