document.addEventListener("DOMContentLoaded", () => {

        document.querySelector('.favorite-toggle').addEventListener('click', ()=>{
        let status = document.querySelector('.favorite-status')
        let icon = document.querySelector('.favorite-icon')
        let productId = document.querySelector('.product-id').value
        const csrf = document.querySelector('.csrf').value

        if (status.value != '') {
            updateFavorite({
                'StockItemID': productId,
                'favorite': false,
                'CSRFtoken': csrf
            }).then(() => {
                icon.textContent = 'favorite_border'
                status.value = ''
            })
        }
        else {
            updateFavorite({
                'StockItemID': productId,
                'favorite': true,
                'CSRFtoken': csrf
            }).then(response => {
                if (response.status === 401) {
                    $('.loginError').removeClass('d-none').toast('show')
                }
                else {
                    icon.textContent = 'favorite'
                    status.value = '1'
                    $('#favoritesDropdown').dropdown('show')
                }
            })
        }
    });

    async function updateFavorite(data) {
        const response = await fetch('/api/favorites', {
            method: 'PUT',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response;
    }

    // document.querySelector('.shoppingcart-trigger').addEventListener('click', ()=>{
    //     let productId = document.querySelector('.product-id').value
    //     const csrf = document.querySelector('.csrf').value
    //
    //     addProductToShoppingCart({
    //         'StockItemID': productId,
    //         'method': '+',
    //         'amount': '+1',
    //         'CSRFtoken': csrf
    //     }).then(() => {
    //         $('#shoppingcartDropdown').dropdown('show')
    //     })
    // });

    function animateValue(element, start, end, duration) {
        if (start === end) return;
        let range = end - start;
        let current = start;
        let increment = end > start? 1 : -1;
        let stepTime = Math.abs(Math.floor(duration / range));
        let timer = setInterval(function() {
            current += increment;
            element.textContent = current;
            if (current == end) {
                clearInterval(timer);
            }
        }, stepTime);
    }

    // async function addProductToShoppingCart(data) {
    //     const response = await fetch('/api/shoppingcart', {
    //         method: 'PUT',
    //         cache: 'no-cache',
    //         credentials: 'same-origin',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify(data)
    //     });
    //     return response;
    // }

    const updateTemp = () => {
        getLiveTemp().then(data => {
            if (data.length > 0) {
                const avg = array => array.reduce((a, b) => a*1 + b*1, 0) / array.length
                document.querySelector('.product-temp').classList.remove('d-none')
                let startVal = document.querySelector('.product-temp-text-live-number').textContent * 1
                let endVal = Math.round(avg(data))
                animateValue(document.querySelector('.product-temp-text-live-number'), startVal, endVal, 1000)
            }
        })
    }

    if (document.querySelector('.temp-enabled-product').value === 'true') {
        window.setInterval(function(){
            updateTemp()
        }, 5000);

        updateTemp()
    }

    async function getLiveTemp() {
        const response = await fetch('/api/get-temperatures', {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'same-origin'
        });
        return response.json();
    }

});