document.addEventListener("DOMContentLoaded", () => {

    const paginationNext = document.querySelectorAll('.paginationNext')
    const paginationPrevious = document.querySelectorAll('.paginationPrevious')

    paginationNext.forEach(e => {
        e.addEventListener('click', event => {
            let page = event.target.dataset.page * 1
            let newPage = page+1
            $('#pills-'+newPage).tab('show')
            load(page)
        })
    })

    paginationPrevious.forEach(e => {
        e.addEventListener('click', event => {
            let page = event.target.dataset.page * 1
            let newPage = page-1
            $('#pills-'+newPage).tab('show')
            unload(newPage)
        })
    })

    const load = (page) => {
        document.querySelector('#loader'+page).classList.remove('progressBarEmpty')
        document.querySelector('#loader'+page).classList.add('progressBarFull')
    }
    const unload = (page) => {
        document.querySelector('#loader'+page).classList.remove('progressBarFull')
        document.querySelector('#loader'+page).classList.add('progressBarEmpty')
    }

})