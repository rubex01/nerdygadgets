document.addEventListener("DOMContentLoaded", () => {

    $('.checkout-overview').on('show.bs.modal', function (e) {
        loadInformation()
    })

    const deliveryadresDefault = document.querySelector('#deliveryAdresDefault')
    const deliveryadresCustom = document.querySelector('#deliveryAdresCustom')

    let deliveryAdresMethod = (document.querySelector('.checkout-card-deliveryaddress-selected').id === 'deliveryAdresDefault') ? 'default' : 'custom';

    deliveryadresCustom.addEventListener('click', () => {
        deliveryadresCustom.classList.add('checkout-card-deliveryaddress-selected')
        if (deliveryAdresMethod === 'default') {
            deliveryadresDefault.classList.remove('checkout-card-deliveryaddress-selected')
        }
        deliveryAdresMethod = 'custom'
    })

    if (deliveryAdresMethod === 'default') {
        deliveryadresDefault.addEventListener('click', () => {
            deliveryadresDefault.classList.add('checkout-card-deliveryaddress-selected')
            deliveryadresCustom.classList.remove('checkout-card-deliveryaddress-selected')
            deliveryAdresMethod = 'default'
        })
    }

    const ideal = document.querySelector('#ideal')
    const paylater = document.querySelector('#payLater')

    let payMethod = (document.querySelector('.checkout-card-pay-selected').id === 'ideal') ? 'ideal' : 'payLater';

    ideal.addEventListener('click', () => {
        ideal.classList.add('checkout-card-pay-selected')
        paylater.classList.remove('checkout-card-pay-selected')
        payMethod = 'ideal'
    })

    paylater.addEventListener('click', () => {
        paylater.classList.add('checkout-card-pay-selected')
        ideal.classList.remove('checkout-card-pay-selected')
        payMethod = 'payLater'
    })

    const loadInformation = () => {
        let info = null;
        if (deliveryAdresMethod == 'custom') {
            info = {
                'address': document.querySelector('#inputAddress').value,
                'house_number': document.querySelector('#inputHousenumber').value,
                'zipcode': document.querySelector('#inputZip').value,
                'domicile': document.querySelector('#inputDomicile').value,
                'province': document.querySelector('#inputState').value
            }
            document.querySelector('#addressField').textContent = info.address + ' ' + info.house_number
            document.querySelector('#zipField').textContent = info.zipcode
            document.querySelector('#domicileField').textContent = info.domicile
        }
        let idealMethod = (payMethod == 'ideal') ? document.querySelector('#bankInput').value : null
        let objectToSend = {
            'paymentMethod': payMethod,
            'idealBank': idealMethod,
            'deliveryAddressMethod': deliveryAdresMethod,
            'deliveryAddressInfo': info
        }
        if (payMethod === 'payLater') {
            document.querySelector('#orderFinishButton').textContent = 'Bestelling plaatsen'
        }
        else {
            document.querySelector('#orderFinishButton').textContent = 'Nu betalen'
        }
        document.querySelector('.object').value = JSON.stringify(objectToSend)
    }
})