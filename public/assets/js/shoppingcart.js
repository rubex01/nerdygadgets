const updateShoppingcart = () => {
    getShoppingcart().then(shoppingcartItems => {
        let table = document.querySelector('#shoppingcartTable')
        let badge = document.querySelector('.shoppingcart-badge')
        let totalCount = 0;
        table.innerHTML = '';
        shoppingcartItems.forEach(shoppingcartItem => {
            let newRow = table.insertRow(0)
            let cell1 = newRow.insertCell(0)
            let cell2 = newRow.insertCell(1)
            let cell3 = newRow.insertCell(2)
            cell1.innerHTML = '<img src="/assets/images/products/StockItemIMG/'+shoppingcartItem.ImagePath+'" class="navigation-list-dropdown-shoppingcart-list-table-img" />'
            cell2.innerHTML = '<a href="/product/' + shoppingcartItem.StockItemID + '" class="navigation-list-dropdown-shoppingcart-list-table-link"><p class="navigation-list-dropdown-shoppingcart-list-table-text">' + shoppingcartItem.StockItemName+'</p></a>'
            cell3.innerHTML = '<span class="navigation-list-dropdown-shoppingcart-list-table-amount">'+shoppingcartItem.amount+'</span>'
            totalCount = totalCount + (shoppingcartItem.amount*1)
        })
        badge.textContent = totalCount
        badge.classList.remove('d-none')
        if (shoppingcartItems.length == 0) {
            let newRow = table.insertRow(0)
            let cell1 = newRow.insertCell(0)
            cell1.innerHTML = '<p class="navigation-list-dropdown-shoppingcart-list-table-noshoppingcartitems">Geen producten...</p>'
            badge.classList.add('d-none')
        }
    })
}

async function getShoppingcart() {
    const response = await fetch('/api/shoppingcart', {
        method: 'GET',
        cache: 'no-cache',
        credentials: 'same-origin'
    });
    return response.json();
}

document.addEventListener("DOMContentLoaded", () => {

    $('#shoppingcartDropdownList').on('show.bs.dropdown', () => {
        updateShoppingcart()
    })

    updateShoppingcart()
});



