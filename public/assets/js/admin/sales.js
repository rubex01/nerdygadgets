document.addEventListener("DOMContentLoaded", () => {

    document.querySelector('#inputProductId').addEventListener('change', event => {
        let productId = event.target.value
        getProductName(productId).then(data => {
            document.querySelector('.sale-product-info').textContent = data.StockItemName
        }).catch(() => {
            document.querySelector('.sale-product-info').textContent = 'Er is geen product met dit artikelnummer'
        });
    })

    async function getProductName(productId) {
        const response = await fetch('/api/productinfo/'+productId, {
            method: 'GET',
            cache: 'no-cache',
            credentials: 'same-origin'
        });
        return response.json();
    }
});