document.addEventListener("DOMContentLoaded", () => {

    if (document.getElementsByClassName('products-grid-page')[0]) {
        document.querySelectorAll('.shoppingcart-trigger').forEach(element => {
            element.addEventListener('click', (e) => {
                if (e.target.dataset.shoppingcart == 'true') {
                    e.preventDefault()

                    let productId = e.target.dataset.id
                    const csrf = document.querySelector('.csrf').value

                    addProductToShoppingCart({
                        'StockItemID': productId,
                        'method': '+',
                        'amount': '+1',
                        'CSRFtoken': csrf
                    }).then(() => {
                        $('#shoppingcartDropdown').dropdown('show')
                    })

                    console.log(productId)
                }
            })
        })
    }
    else {
        document.querySelector('.shoppingcart-trigger').addEventListener('click', ()=>{
            let productId = document.querySelector('.product-id').value
            const csrf = document.querySelector('.csrf').value

            addProductToShoppingCart({
                'StockItemID': productId,
                'method': '+',
                'amount': '+1',
                'CSRFtoken': csrf
            }).then(() => {
                $('#shoppingcartDropdown').dropdown('show')
            })
        });
    }

    async function addProductToShoppingCart(data) {
        const response = await fetch('/api/shoppingcart', {
            method: 'PUT',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        return response;
    }
});
