document.addEventListener("DOMContentLoaded", () => {

    if (document.querySelector('.login-status').value == 'true') {
        document.querySelector('.order-button').addEventListener('click', (e) => {
            e.target.classList.add('order-animation')
            const location = document.querySelector('.order-button').dataset.location
            setTimeout(() => {
                window.location.href = location;
            }, 1000);
        })
    }

    const element = document.querySelectorAll('.amount-input')
    let updateState = false;

    element.forEach(function(el){
        el.addEventListener('change', (e) => {
            updateState = e.target.value
            const staticVal = updateState
            setTimeout(() => {
                if (updateState === staticVal) {
                    updateShoppingCartAmmount(e.target)
                }
            }, 800);
        })
        el.addEventListener('keyup', (e) => {
            updateState = e.target.value
            const staticVal = updateState
            setTimeout(() => {
                if (updateState === staticVal) {
                    updateShoppingCartAmmount(e.target)
                }
            }, 800);
        })
    });

    const updateShoppingCartAmmount = (target) => {
        let productId = target.dataset.id
        let amount = target.value * 1;
        let csrf = document.querySelector('.csrf').value
        if (amount > 0 && amount < 11) {
            let data = {
                'StockItemID': productId,
                'method': '+',
                'amount': amount,
                'CSRFtoken': csrf
            }
            newShoppingCartAmount(data, target)
        } else {
            target.value = (amount > 10) ? 10 : 1
        }
    }

    async function newShoppingCartAmount(data, target) {
        const response = await fetch('/api/shoppingcart', {
            method: 'PUT',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(() => {
            target.value = data.amount
            target.blur()
            let price = document.querySelector('#productPrice'+data.StockItemID).dataset.price * 1
            let newPrice = Math.round(((price * data.amount) + Number.EPSILON) * 100) / 100;
            let newPriceString = newPrice.toString();
            let finalPrice = newPriceString.replace('.', ',')
            document.querySelector('#productPrice'+data.StockItemID+' span').textContent = finalPrice
            updateShoppingcart()
            calculateTotalPrices()
        });
        return response;
    }

    const calculateTotalPrices = () => {
        let totalProducts = 0;
        let shippingCost = 0;

        element.forEach(function(el){
            let amount = el.value * 1
            let singleItemPrice = document.querySelector('#productPrice'+el.dataset.id).dataset.price * 1
            let totalItemPrice = amount * singleItemPrice
            totalProducts = totalProducts + totalItemPrice
        })

        let total = totalProducts + shippingCost;

        let priceRound = Math.round((totalProducts + Number.EPSILON) * 100) / 100;
        let priceString = priceRound.toString();
        let priceStringVisual = priceString.replace('.', ',')
        document.querySelector('.total-products-price').textContent = '€ '+priceStringVisual

        let totalRound = Math.round((total + Number.EPSILON) * 100) / 100;
        let totalString = totalRound.toString();
        let totalStringVisual = totalString.replace('.', ',')
        document.querySelector('.total-price').textContent = '€ '+totalStringVisual
    }

    calculateTotalPrices()
});
