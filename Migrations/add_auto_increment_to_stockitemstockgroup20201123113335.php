<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_auto_increment_to_stockitemstockgroup20201123113335 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `stockitemstockgroups` CHANGE `StockItemStockGroupID` `StockItemStockGroupID` INT(11) NOT NULL AUTO_INCREMENT");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `stockitemstockgroups` CHANGE `StockItemStockGroupID` `StockItemStockGroupID` INT(11) NOT NULL");
    }
}
