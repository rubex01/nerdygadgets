<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class insert_default_user20201205184923 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql('
            insert into users 
            (email, password, phone_number, role, gender, first_name, insertion, last_name, address, house_number, zipcode, domicile, province, date_of_birth, newsletter, agreement) 
            values 
            ("test@test.nl", "$2y$10$yKJQS6dMN/eOAbSEK38LdOxWQ90M12lVsiOTJGpXVHYpq0RcxnNj.", "0612345678", null, "man", "test", "", "test", "address", "12", "3333 LK", "woonplaats", "gelderland", "2001-01-01", null, 1)
        ');
    }

    public function down(): void
    {
        $this->addSql("delete from users where email = test@test.nl");
    }
}
