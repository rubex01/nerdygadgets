<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_deliverytime_to_stockitems20201119135711 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `stockitems` ADD `deliverytime` INT DEFAULT NULL AFTER `description`;");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `stockitems` DROP `deliverytime`");
    }
}
