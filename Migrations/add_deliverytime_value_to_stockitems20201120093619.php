<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_deliverytime_value_to_stockitems20201120093619 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("update stockitems set deliverytime = 1");
    }

    public function down(): void
    {
        $this->addSql("update stockitems set deliverytime = 0");
    }
}
