<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class create_uptodate_table20201201082739 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("
            CREATE TABLE `uptodate` ( 
            `uptodate_key` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `StockItemID` int(11) NOT NULL,
            PRIMARY KEY (uptodate_key),
            FOREIGN KEY (user_id) REFERENCES users(user_id),
            FOREIGN KEY (StockItemID) REFERENCES stockitems(StockItemID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ");
    }

    public function down(): void
    {
        $this->addSql("drop table uptodate");
    }
}
