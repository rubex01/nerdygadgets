<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class contact20201116210357 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("
            CREATE TABLE `contact` ( 
            `contact_id` int(11) NOT NULL AUTO_INCREMENT,
            `email` VARCHAR(45) NOT NULL,
            `name` VARCHAR(45) NOT NULL,
            `subject` VARCHAR(145) NOT NULL,
            `text` TEXT NOT NULL, 
            PRIMARY KEY (contact_id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ");
    }

    public function down(): void
    {
        $this->addSql("DROP TABLE contact");
    }
}
