<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class password_reset_links20201127222229 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("CREATE TABLE `password_reset_links` (
            `password_reset_id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `link` VARCHAR(255) NOT NULL, 
            `valid_to` DATETIME NOT NULL,
            PRIMARY KEY (password_reset_id),
            FOREIGN KEY (user_id) REFERENCES users(user_id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
    }

    public function down(): void
    {
        $this->addSql("drop table password_reset_links");
    }
}
