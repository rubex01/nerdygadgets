<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class make_photo_varchar20201206142317 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `people` CHANGE `Photo` `Photo` VARCHAR(255) NULL;");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `people` CHANGE `Photo` `Photo` LONGBLOB NULL DEFAULT NULL;");
    }
}
