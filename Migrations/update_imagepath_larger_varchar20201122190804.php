<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class update_imagepath_larger_varchar20201122190804 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `stockitemimages` CHANGE `ImagePath` `ImagePath` VARCHAR(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `stockitemimages` CHANGE `ImagePath` `ImagePath` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
    }
}
