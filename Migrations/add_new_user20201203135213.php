<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_new_user20201203135213 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("CREATE USER 'webshopuser'@'localhost' IDENTIFIED BY '".getenv('DB_WEBSHOP_USER_PASSWORD')."'");
        $this->addSql("GRANT ALL ON nerdygadgets.admin_user_sessions TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.checkout TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.checkout_products TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.coldroomtemperatures TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.coldroomtemperatures_archive TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.contact TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.favorites TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.password_reset_links TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.people TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.retour TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.reviews TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.shoppingcart TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.specialdeals TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.stockgroups TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.stockitemholdings TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.stockitemimages TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.stockitemstockgroups TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.uptodate TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.stockitems TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.users TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.user_sessions TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.userstockiteminterest TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT SELECT ON nerdygadgets.colors TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT SELECT ON nerdygadgets.packagetypes TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT SELECT ON nerdygadgets.suppliercategories TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT SELECT ON nerdygadgets.suppliers TO 'webshopuser'@'localhost'");
        $this->addSql("GRANT ALL ON nerdygadgets.contactemails TO 'webshopuser'@'localhost'");
    }

    public function down(): void
    {
        $this->addSql("DROP USER 'webshopuser'@'localhost'");
    }
}
