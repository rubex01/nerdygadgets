<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_default_admin_user20201217225421 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $hashedPassword = hash('sha256', 'NerdyGadgetsAdmin'.getenv('ADMIN_SALT'));
        $this->addSql("insert into people 
                (FullName, PreferredName, SearchName, IsPermittedToLogon, LogonName, IsExternalLogonProvider, HashedPassword, IsSystemUser, IsEmployee, IsSalesPerson, UserPreferences, PhoneNumber, FaxNumber, EmailAddress, Photo, CustomFields, OtherLanguages, LastEditedBy, ValidFrom, ValidTo, setup_token) values 
                ('NerdyGadgets Admin', 'Admin', 'Admin NerdyGadgets Admin', 1, 'admin@nerdygadgets.nl', 0, '$hashedPassword', 1, 1, 1, '', '0612345678', '', 'admin@nerdygadgets.nl', 'default.png', '', '', 1, '2020-02-02 00:00:00', '2025-02-02 00:00:00', null)");
    }

    public function down(): void
    {
        $this->addSql("delete from people where EmailAddress = 'admin@nerdygadgets.nl'");
    }
}
