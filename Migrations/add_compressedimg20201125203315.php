<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_compressedimg20201125203315 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("update stockgroups set ImagePath = 'chocolate.webp' where StockGroupID = 1");
        $this->addSql("update stockgroups set ImagePath = 'Clothing.webp' where StockGroupID = 2");
        $this->addSql("update stockgroups set ImagePath = 'MugsNovelties.webp' where StockGroupID = 3");
        $this->addSql("update stockgroups set ImagePath = 'T-shirts.webp' where StockGroupID = 4");
        $this->addSql("update stockgroups set ImagePath = 'AirlineNovelties.webp' where StockGroupID = 5");
        $this->addSql("update stockgroups set ImagePath = 'ComputingNovelties.webp' where StockGroupID = 6");
        $this->addSql("update stockgroups set ImagePath = 'USBNovelties.webp' where StockGroupID = 7");
        $this->addSql("update stockgroups set ImagePath = 'Toys.webp' where StockGroupID = 9");
    }

    public function down(): void
    {
        $this->addSql("update stockgroups set ImagePath = 'chocolate.jpg' where StockGroupID = 1");
        $this->addSql("update stockgroups set ImagePath = 'Clothing.jpg' where StockGroupID = 2");
        $this->addSql("update stockgroups set ImagePath = NULL where StockGroupID = 3");
        $this->addSql("update stockgroups set ImagePath = 'T-shirts.jpg' where StockGroupID = 4");
        $this->addSql("update stockgroups set ImagePath = 'AirlineNovelties.jpg' where StockGroupID = 5");
        $this->addSql("update stockgroups set ImagePath = 'ComputingNovelties.jpg' where StockGroupID = 6");
        $this->addSql("update stockgroups set ImagePath = 'USBNovelties.jpg' where StockGroupID = 7");
        $this->addSql("update stockgroups set ImagePath = 'Toys.jpg' where StockGroupID = 9");
    }
}
