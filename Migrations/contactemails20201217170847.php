<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class contactemails20201217170847 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql(" 
        CREATE TABLE `contactemails` ( 
            `contactemails_id` int(11) NOT NULL AUTO_INCREMENT,
            `email` VARCHAR(255) NOT NULL,
            PRIMARY KEY (contactemails_id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"
        );
    }

    public function down(): void
    {
        $this->addSql("DROP TABLE contactemails");
    }
}
