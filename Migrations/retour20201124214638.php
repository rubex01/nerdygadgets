<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class retour20201124214638 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("CREATE TABLE `retour` (
            `retour_id` int(11) NOT NULL AUTO_INCREMENT,
            `checkout_product_id` int(11) NOT NULL,
            `amount` int(11) NOT NULL,
            `reason` VARCHAR(255) NOT NULL, 
            `retour_time` DATETIME NOT NULL,
            PRIMARY KEY (retour_id),
            FOREIGN KEY (checkout_product_id) REFERENCES checkout_products(checkout_product_id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
    }

    public function down(): void
    {
        $this->addSql("drop table retour");
    }
}
