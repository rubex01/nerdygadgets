<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class checkout_products20201119145915 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("
            CREATE TABLE `checkout_products` (
            `checkout_product_id` int(11) NOT NULL AUTO_INCREMENT,
            `StockItemID` int(11) NOT NULL,
            `checkout_id` INT(11) NOT NULL,
            `amount` int(11) NOT NULL,
            `price` decimal(18,2) NOT NULL, 
            PRIMARY KEY (checkout_product_id),
            FOREIGN KEY (StockItemID) REFERENCES stockitems(StockItemID),
            FOREIGN KEY (checkout_id) REFERENCES checkout(checkout_id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ");
    }

    public function down(): void
    {
        $this->addSql("Drop table checkout_products");
    }
}
