<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_trigger_after_instert_add_stockitemholdings20201206211201 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("CREATE TRIGGER add_to_stockitemholdings AFTER INSERT ON stockitems FOR EACH ROW BEGIN INSERT INTO stockitemholdings (StockItemID, QuantityOnHand, BinLocation, LastStocktakeQuantity, LastCostPrice, ReorderLevel, TargetStockLevel, LastEditedBy, LastEditedWhen) values (new.StockItemID, 0, '', 0, new.UnitPrice, 5, 50, new.LastEditedBy, now()); END");
    }

    public function down(): void
    {
        $this->addSql("drop trigger add_to_stockitemholdings");
    }
}
