<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_auto_increment_to_specialdeals20201130121008 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `specialdeals` CHANGE `SpecialDealID` `SpecialDealID` INT(11) NOT NULL AUTO_INCREMENT;");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `specialdeals` CHANGE `SpecialDealID` `SpecialDealID` INT(11) NOT NULL;");
    }
}
