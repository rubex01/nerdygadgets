<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_setup_token_to_people20201206154142 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `people` ADD `setup_token` VARCHAR(255) NULL DEFAULT NULL AFTER `ValidTo`");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `people` DROP `setup_token`;");
    }
}
