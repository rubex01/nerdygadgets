<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class users20200921080920 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("
                CREATE TABLE `users` (
                  `user_id` int(11) NOT NULL AUTO_INCREMENT,
                  `email` varchar(255) NOT NULL,
                  `password` varchar(255) NOT NULL,
                  `phone_number` varchar(45) NOT NULL,
                  `role` varchar(255) DEFAULT NULL,
                  `gender` varchar(255) DEFAULT NULL,
                  `first_name` varchar(255) DEFAULT NULL,
                  `insertion` varchar(255) DEFAULT NULL,
                  `last_name` varchar(255) DEFAULT NULL,
                  `address` varchar(255) DEFAULT NULL,
                  `house_number` varchar(255) DEFAULT NULL,
                  `zipcode` varchar(45) DEFAULT NULL,
                  `domicile` varchar(255) DEFAULT NULL,
                  `province` varchar(255) DEFAULT NULL,
                  `date_of_birth` DATETIME NULL DEFAULT NULL,
                  `newsletter` BOOLEAN DEFAULT NULL,
                  `agreement` BOOLEAN NOT NULL , 
                  PRIMARY KEY (user_id)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
            ");
    }

    public function down(): void
    {
        $this->addSql("DROP TABLE users");
    }
}
