<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_procedure_reset_coldroom20201202201424 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("
            CREATE DEFINER=`root`@`localhost` PROCEDURE `reset_coldroom_table`()
            BEGIN
            insert into coldroomtemperatures_archive select * from coldroomtemperatures;
            delete from coldroomtemperatures;
            END
        ");
    }

    public function down(): void
    {
        $this->addSql("DROP PROCEDURE `nerdygadgets`.`reset_coldroom_table`");
    }
}
