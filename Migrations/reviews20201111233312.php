<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class reviews20201111233312 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("
            CREATE TABLE `reviews` ( 
            `review_id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `StockItemID` int(11) NOT NULL,
            `text` TEXT NOT NULL , 
            `stars` INT(1) NOT NULL,
            PRIMARY KEY (review_id),
            FOREIGN KEY (user_id) REFERENCES users(user_id),
            FOREIGN KEY (StockItemID) REFERENCES stockitems(StockItemID)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
        ");
    }

    public function down(): void
    {
        $this->addSql("DROP TABLE reviews");
    }
}
