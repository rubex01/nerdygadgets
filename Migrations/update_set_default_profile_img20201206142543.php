<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class update_set_default_profile_img20201206142543 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("update people set photo = 'default.png'");
    }

    public function down(): void
    {
        $this->addSql("update people set photo = ''");
    }
}
