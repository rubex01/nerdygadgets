<?php

namespace Migrations;

use Framework\Migrations\Migrations;
use Framework\Migrations\MigrationInterface;

class add_description_to_stockitems20201111231343 extends Migrations implements MigrationInterface
{
    public function up(): void
    {
        $this->addSql("ALTER TABLE `stockitems` ADD `description` TEXT DEFAULT NULL AFTER `StockItemName`;");
    }

    public function down(): void
    {
        $this->addSql("ALTER TABLE `stockitems` DROP `description`;");
    }
}
