# from sense_hat import SenseHat 
import time 
import json 
import requests 
import datetime
import random

# sense = SenseHat()
temps = list()
ipaddress = 'Ip address of your computer'
token = 'same token as in the TEMP_TOKEN in your .env file'

while True:
    # Choose one of the following depending on if you have a sensehat or not:
	# option 1: currentTemp = sense.get_temperature()
	# option 2: currentTemp = random.sample(range(0, 5), 1)[0]
	print 'temp calc' 
	temps.append(currentTemp) 
	if len(temps) > 3: 
		print 'sending request'
		st = time.time()
		currentTimeStamp = datetime.datetime.fromtimestamp(st).strftime('%Y-%m-%d %H:%M:%S')
		sendObj = {}
		sendObj['token'] = token
    		sendObj["temperatures"] = list() 
		for i in temps:
      			sendObj["temperatures"].append({ 
				"ColdRoomSensorNumber": 1, 
				"RecordedWhen": currentTimeStamp, 
				"Temperature": i
   			})
    		x = requests.post('http://'+ipaddress+'/api/register-temp', data = json.dumps(sendObj), headers={"Content-Type":"application/json"})
		del temps[:] 
		del sendObj
		print 'Updated temp' 
 	time.sleep(1)
