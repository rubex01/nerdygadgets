<?php

namespace App\Middleware;

use Framework\CustomPackages\AdminAuth\AdminAuth;

class AdminAuthentication
{
    use \App\Controllers\NotificationTrait;
    
    public function handle() : bool
    {
        return AdminAuth::check();
    }

    public function onFailure()
    {
        $this->addNotification('Geen toegang', 'U moet inloggen om deze pagina te kunnen bezoeken', 'warning');
        header('location:/admin/login');
    }
}