<?php

namespace App\Middleware;

use Framework\Auth\Auth;
use Framework\Database\Database;

class ShoppingCartFull
{
    use \App\Controllers\NotificationTrait;

    public function handle() : bool
    {
        $stmt = Database::$Connections['MySQL']->prepare(
            "select shopping_id from shoppingcart where user_id = ?"
        );
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            return true;
        }
        return false;
    }

    public function onFailure()
    {
        $this->addNotification('Geen toegang', 'Stop alstublieft items in uw winkelwagen voor u afrekent', 'error');
        header('location:/');
    }
}