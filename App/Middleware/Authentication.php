<?php

namespace App\Middleware;

use Framework\Auth\Auth;

class Authentication
{
    use \App\Controllers\NotificationTrait;

    public function handle() : bool
    {
        return Auth::check();
    }

    public function onFailure()
    {
        $this->addNotification('Geen toegang', 'U moet inloggen om deze pagina te kunnen bezoeken', 'warning');
        header('location:/');
    }
}