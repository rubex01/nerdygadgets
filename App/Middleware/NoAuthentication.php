<?php

namespace App\Middleware;

use Framework\Auth\Auth;

class NoAuthentication
{
    use \App\Controllers\NotificationTrait;

    public function handle() : bool
    {
        return !Auth::check();
    }

    public function onFailure()
    {
        $this->addNotification('Geen toegang', 'U bent al ingelogd en mag deze pagina niet bezoeken', 'error');
        header('location:/');
    }
}