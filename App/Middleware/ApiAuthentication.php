<?php

namespace App\Middleware;

use Framework\Auth\Auth;

class ApiAuthentication
{
    public function handle() : bool
    {
        return Auth::check();
    }

    public function onFailure()
    {
        header('Content-type: application/json');
        http_response_code(401);
        echo json_encode([['message' => 'U moet ingelogd zijn voor deze actie']]);
    }
}