<?php

namespace App;

class Config
{
    public static $middleware = [];

    public static $routeMiddleware = [
        'auth' => \App\Middleware\Authentication::class,
        'noAuth' => \App\Middleware\NoAuthentication::class,
        'apiAuth' => \App\Middleware\ApiAuthentication::class,
        'shoppingCartFull' => \App\Middleware\ShoppingCartFull::class,
        'adminAuth' => \App\Middleware\AdminAuthentication::class
    ];

    public static $exceptionHandler = [

    ];

    public static $enabledFunctions = [
        'authorization' => true,
        'adminAuthorization' => true
    ];
}