<?php

namespace App\Controllers\Admin\Products;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;

class EditProductController
{
    use ProductInfoTrait;

    public function getEditProduct(Request $request, $productId)
    {
        $productInfo = $this->getProductInfo($productId);
        $allCategories = $this->getAllCategories();
        $allColors = $this->getAllColors();
        $suppliers = $this->getAllSuppliers();
        $packageTypes = $this->getAllPackageTypes();

        $productImages = [];
        $productCategories = [];

        foreach ($productInfo as $productRow) {
            if (!in_array($productRow['ImagePath'], $productImages)) {
                $productImages[] = $productRow['ImagePath'];
            }
            $currentCategory = ['categoryId' => $productRow['StockGroupID'], 'categoryName' => $productRow['StockGroupName']];
            if (!in_array($currentCategory, $productCategories)) {
                $productCategories[] = $currentCategory;
            }
        }

        return Pages::view('admin', 'admin/products/edit', [
            'pageTitle' => 'Producten',
            'loggedInUser' => AdminAuth::$user,
            'productImages' => $productImages,
            'productCategories' => $productCategories,
            'productInfo' => $productInfo[0],
            'sass' => ['admin/product-edit.css'],
            'allCategories' => $allCategories,
            'allColors' => $allColors,
            'allSuppliers' => $suppliers,
            'allPackageTypes' => $packageTypes,
            'productInfoEndpoint' => '/admin/endpoint/products/edit/'.$productId
        ]);
    }

    public function getProductInfo(int $productId)
    {
        $product = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                select sig.StockGroupID, StockGroupName, QuantityOnHand, si.*, sim.ImagePath
                from stockitems si 
                left join colors co on co.ColorID = si.ColorID 
                left join suppliers su on su.SupplierID = si.SupplierID 
                left join stockitemimages sim on sim.StockItemId = si.StockItemId
                left join stockitemholdings sih on sih.StockItemID = si.StockItemID
                left join stockitemstockgroups sig on sig.StockItemID = si.StockItemID
                left join stockgroups sg on sg.StockGroupID = sig.StockGroupID
                left join reviews r on r.StockItemID = si.StockItemID
                where si.StockItemID = ?
            ");
        $stmt->bind_param('i', $productId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $product[] = $row;
        }
        return $product;
    }
}