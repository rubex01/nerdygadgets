<?php

namespace App\Controllers\Admin\Products;

use Framework\Database\Database;
use Framework\Request\Request;

class DeleteController
{
    use \App\Controllers\NotificationTrait;

    public function deleteProduct(Request $request, $productId)
    {
        $newDate = date("Y-m-d H:m:s");

        $stmt = Database::$Connections['MySQL']->prepare("update stockitems set ValidTo = ? where StockItemID = ?");
        $stmt->bind_param('si', $newDate, $productId);
        $stmt->execute();

        $this->addNotification('Succes', 'Product is succesvol verwijderd', 'success');
        header('location:/admin/products');
    }
}