<?php

namespace App\Controllers\Admin\Products;

use Framework\Database\Database;
use Framework\Request\Request;
use Framework\Storage\Storage;
use Framework\Validation\Validation;

class ImageController
{
    use \App\Controllers\NotificationTrait;

    public function removeImage($productId, $imagePath)
    {
        $stmt = Database::$Connections['MySQL']->prepare("delete from stockitemimages where StockItemID = ? and ImagePath = ?");
        $stmt->bind_param('is', $productId, $imagePath);
        $stmt->execute();

        $this->addNotification('Success', 'Afbeelding is verwijderd van product', 'success');
        header('location:/admin/products/edit/'.$productId);
    }

    public function addImage(Request $request, $productId)
    {
        $validation = new Validation(
            $request->body,
            [
                'image' => ['file', 'image', 'size:500000'],
            ],
            [
                'image.file' => 'Er ging iets fout',
                'image.image' => 'De afbeelding was niet geldig',
                'image.size:500000' => 'De afbeelding is te groot'
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/products/edit/'.$productId);
            exit();
        }

        $upload = Storage::upload($request->input('image'), true, '/images/products/StockItemIMG/', false);

        $stmt = Database::$Connections['MySQL']->prepare("insert into stockitemimages (StockItemID, ImagePath) values (?, ?)");
        $stmt->bind_param('is', $productId, $upload['fileName']);
        $stmt->execute();


        $this->addNotification('Success', 'Afbeelding is toegevoegd aan product', 'success');
        header('location:/admin/products/edit/'.$productId);
    }
}