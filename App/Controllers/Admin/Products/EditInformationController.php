<?php

namespace App\Controllers\Admin\Products;

use Framework\Database\Database;
use Framework\Request\Request;
use Framework\Validation\Validation;

class EditInformationController
{
    use \App\Controllers\NotificationTrait;
    use ProductInfoTrait;

    public function updateInformation(Request $request, $productId)
    {
        $this->validateInput($request, '/admin/products/edit/'.$productId);

        $newProductInfo = array_values($request->body);
        $newProductInfo[22] = $productId;

        $stmt = Database::$Connections['MySQL']->prepare("
            update stockitems set 
            StockItemName = ?, 
            description = ?,
            deliverytime = ?,
            SupplierID = ?,
            ColorID = ?,
            UnitPackageID = ?,
            OuterPackageID = ?,
            Brand = ?,
            Size = ?,
            QuantityPerOuter = ?,
            IsChillerStock = ?,
            Barcode = ?,
            TaxRate = ?,
            UnitPrice = ?,
            RecommendedRetailPrice = ?,
            TypicalWeightPerUnit = ?,
            MarketingComments = ?,
            InternalComments = ?,
            SearchDetails = ?,
            ValidFrom = ?,
            ValidTo = ?,
            Video = ?
            where StockItemID = ?
        ");
        $stmt->bind_param('ssiiiiissiisssssssssssi', ...$newProductInfo);
        $stmt->execute();

        $this->addNotification('Success', 'Product is succesvol gewijzigd', 'success');
        header('location:/admin/products/edit/'.$productId);
    }
}