<?php

namespace App\Controllers\Admin\Products;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;

class ProductController
{
    public function getProducts(Request $request)
    {
        $query = $request->input('q');
        $page = $request->input('p');

        $queryEnabled = $query !== null ? true : false;
        $limit1 = $page !== null ? $page*30 : 0;
        $limit2 = $limit1+30;

        $totalPages = $this->getTotalPagesCount($query, $queryEnabled) / 30;
        $products = $this->getProductData($query, $queryEnabled, $limit1, $limit2);

        return Pages::view('admin', 'admin/products/products', [
            'pageTitle' => 'Producten',
            'loggedInUser' => AdminAuth::$user,
            'sass' => ['admin/products.css'],
            'js' => ['admin/pagination.js'],
            'user' => AdminAuth::$user,
            'products' => $products,
            'totalPages' => $totalPages,
            'query' => $query,
            'pageNr' => $page
        ]);
    }

    public function getProductData($query, $queryEnabled, $limit1, $limit2)
    {
        $products = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                select si.StockItemID, QuantityOnHand, StockItemName, SupplierName, RecommendedRetailPrice, ImagePath, si.validTo
                from stockitems si 
                left join colors co on co.ColorID = si.ColorID 
                left join suppliers su on su.SupplierID = si.SupplierID 
                left join stockitemimages sim on sim.StockItemId = si.StockItemId
                left join stockitemholdings sih on sih.StockItemID = si.StockItemID
                where (StockItemName like concat('%',?,'%') or MarketingComments like concat('%',?,'%')) or 0 = ?
                group by si.StockItemID
                order by QuantityOnHand
                limit ?, ?
            ");
        $stmt->bind_param('ssiii', $query, $query, $queryEnabled, $limit1, $limit2);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $products[] = $row;
        }
        return $products;
    }

    public function getTotalPagesCount($query, $queryEnabled)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
            select count(StockItemID) count from stockitems where (StockItemName like concat('%',?,'%') or MarketingComments like concat('%',?,'%')) or 0 = ?
        ");
        $stmt->bind_param('ssi', $query, $query, $queryEnabled);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $count = $row['count'];
        }
        return $count;
    }
}