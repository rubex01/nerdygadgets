<?php

namespace App\Controllers\Admin\Products;

use Framework\Database\Database;
use Framework\Request\Request;

class CategoryController
{
    use \App\Controllers\NotificationTrait;

    public function removeTag(Request $request, $productId, $categoryId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("delete from stockitemstockgroups where StockItemID = ? and StockGroupID = ?");
        $stmt->bind_param('ii', $productId, $categoryId);
        $stmt->execute();

        $this->addNotification('Succes', 'Categorie is verwijderd van product', 'success');
        header('location:/admin/products/edit/'.$productId);
    }

    public function addCategory(Request $request, $productId)
    {
        $currentDateTime = date('Y-m-d H:i:s', time());

        $category = $request->input('category');
        $stmt = Database::$Connections['MySQL']->prepare("insert into stockitemstockgroups (StockItemID, StockGroupID, LastEditedBy, LastEditedWhen) values (?, ?, 1, ?)");
        $stmt->bind_param('iis', $productId, $category, $currentDateTime);
        $stmt->execute();

        $this->addNotification('Succes', 'Categorie is toegevoegd aan product', 'success');
        header('location:/admin/products/edit/'.$productId);
    }
}