<?php

namespace App\Controllers\Admin\Products;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;

class AddProductController
{
    use ProductInfoTrait;
    use \App\Controllers\NotificationTrait;

    public function getAddProduct()
    {
        $allCategories = $this->getAllCategories();
        $allColors = $this->getAllColors();
        $suppliers = $this->getAllSuppliers();
        $packageTypes = $this->getAllPackageTypes();

        return Pages::view('admin', 'admin/products/add', [
            'pageTitle' => 'Producten',
            'loggedInUser' => AdminAuth::$user,
            'productInfoEndpoint' => '/admin/endpoint/products/add',
            'allCategories' => $allCategories,
            'allColors' => $allColors,
            'allSuppliers' => $suppliers,
            'allPackageTypes' => $packageTypes
        ]);
    }

    public function storeProduct(Request $request)
    {
        $this->validateInput($request, '/admin/products/add');

        $newProductInfo = array_values($request->body);
        unset($newProductInfo[22]);

        $stmt = Database::$Connections['MySQL']->prepare("select max(StockItemID) StockItemID from stockitems");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $newStockItemID = $row['StockItemID']+1;
        }

        $stmt = Database::$Connections['MySQL']->prepare("
            insert into stockitems (
                StockItemName, 
                description, 
                deliverytime, 
                SupplierID, 
                ColorID, 
                UnitPackageID, 
                OuterPackageID, 
                Brand, 
                Size, 
                QuantityPerOuter, 
                IsChillerStock, 
                Barcode, 
                TaxRate, 
                UnitPrice, 
                RecommendedRetailPrice, 
                TypicalWeightPerUnit, 
                MarketingComments, 
                InternalComments, 
                SearchDetails, 
                ValidFrom, 
                ValidTo, 
                Video, 
                
                StockItemID,
                LeadTimeDays,
                CustomFields,
                Tags,
                LastEditedBy
            ) 
            values
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ".$newStockItemID.", 14, '{}', '[]', ".AdminAuth::$id.");
        ");
        $stmt->bind_param('sssiiiissiisssssssssss', ...$newProductInfo);
        $stmt->execute();

        $this->addNotification('Success', 'Product is toegevoegd', 'success');
        header('location:/admin/products/edit/'.$newStockItemID);
    }
}