<?php

namespace App\Controllers\Admin\Products;

use Framework\Database\Database;
use Framework\Validation\Validation;

trait ProductInfoTrait
{
    public function getAllPackageTypes()
    {
        $packageTypes = [];
        $stmt = Database::$Connections['MySQL']->prepare("select PackageTypeID, PackageTypeName from packagetypes");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $packageTypes[] = $row;
        }
        return $packageTypes;
    }

    public function getAllSuppliers()
    {
        $suppliers = [];
        $stmt = Database::$Connections['MySQL']->prepare("select SupplierID, SupplierName from suppliers");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $suppliers[] = $row;
        }
        return $suppliers;
    }

    public function getAllCategories()
    {
        $categories = [];
        $stmt = Database::$Connections['MySQL']->prepare("select StockGroupID, StockGroupName from stockgroups");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }
        return $categories;
    }

    public function getAllColors()
    {
        $allColors = [];
        $stmt = Database::$Connections['MySQL']->prepare("select ColorID, ColorName from colors");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $allColors[] = $row;
        }
        return $allColors;
    }

    public function validateInput($request, $returnURL)
    {
        $validation = new Validation(
            $request->body,
            [
                'StockItemName' => ['required', 'string'],
                'description' => ['string'],
                'deliverytime' => ['required'],
                'SupplierID' => ['required', 'exists:suppliers'],
                'ColorID' => ['required', 'exists:colors'],
                'UnitPackageID' => ['required'],
                'OuterPackageID' => ['required'],
                'Brand' => ['string'],
                'Size' => ['string'],
                'QuantityPerOuter' => ['required', 'string'],
                'IsChillerStock' => ['required'],
                'Barcode' => ['string'],
                'TaxRate' => ['required'],
                'UnitPrice' => ['required'],
                'RecommendedRetailPrice' => ['required'],
                'TypicalWeightPerUnit' => ['required'],
                'MarketingComments' => ['string'],
                'InternalComments' => ['string'],
                'SearchDetails' => ['required', 'string'],
                'ValidFrom' => ['required'],
                'ValidTo' => ['required'],
                'Video' => ['string']
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:'.$returnURL);
            exit();
        }
    }
}