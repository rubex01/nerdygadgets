<?php

namespace App\Controllers\Admin\Auth;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;

class SetupController
{
    use \App\Controllers\NotificationTrait;

    public function getSetup(Request $request, $token)
    {
        return Pages::view('empty', 'admin/auth/setup', [
            'sass' => ['admin/login.css'],
            'token' => $token
        ]);
    }

    public function setupAction(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'passord' => ['required', 'confirmation'],
                'setup_token' => ['required', 'exists:people']
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/login');
            exit();
        }

        $newPassword = hash('sha256', $request->input('password').getenv('ADMIN_SALT'));
        $token = $request->input('setup_token');

        $stmt = Database::$Connections['MySQL']->prepare("update people set HashedPassword = ?, setup_token = null where setup_token = ?");
        $stmt->bind_param('ss', $newPassword, $token);
        $stmt->execute();

        $this->addNotification('Succes', 'Wachtwoord is succesvol ingesteld', 'success');
        header('location:/admin/login');
    }
}