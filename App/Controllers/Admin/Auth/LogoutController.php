<?php

namespace App\Controllers\Admin\Auth;

use Framework\CustomPackages\AdminAuth\AdminAuth;

class LogoutController
{
    use \App\Controllers\NotificationTrait;

    public function AdminLogout()
    {
        AdminAuth::logout();
        $this->addNotification('Succes', 'U bent succesvol uitgelogd', 'success');
        header('location:/admin/login');
    }
}