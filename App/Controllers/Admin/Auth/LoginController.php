<?php

namespace App\Controllers\Admin\Auth;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Validation\Validation;

class LoginController
{
    use \App\Controllers\RecaptchaTrait;
    use \App\Controllers\NotificationTrait;

    public function getLogin()
    {
        return Pages::view('empty', 'admin/auth/login', [
            'sass' => ['admin/login.css'],
        ]);
    }

    public function loginAction(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'EmailAddress' => ['required', 'email', 'exists:people'],
                'password' => ['required', 'string'],
                'g-recaptcha-response' => ['required']
            ],
            [
                'EmailAddress.required' => 'Email is verplicht',
                'EmailAddress.email' => 'Gebruik een geldig email adres',
                'EmailAddress.exists:users' => 'Er bestaat geen account met dit email adres',
                'password.required' => 'Wachtwoord is verplicht',
                'g-recaptcha-response.required' => 'reCAPTCHA is verplicht'
        ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/login');
            exit();
        }

        $this->recaptchaValidation($request->input('g-recaptcha-response'), '/admin/login');

        $login = AdminAuth::login(['email' => $request->input('EmailAddress'), 'password' => $request->input('password')]);

        if ($login === true) {
            $this->addNotification('Ingelogd', 'U bent ingelogd', 'success');
            header('location:/admin');
        }
        else {
            $this->addNotification('Niet ingelogd', 'Het wachtwoord komt niet overeen', 'error');
            header('location:/admin/login');
        }
    }
}