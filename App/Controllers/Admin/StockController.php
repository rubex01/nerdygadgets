<?php

namespace App\Controllers\Admin;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Pages\Pages;
use Framework\Database\Database;
use Framework\Request\Request;
use App\Controllers\Products\UptoDateController;

class StockController
{
    use \App\Controllers\NotificationTrait;

    public function getstockAction()
    {
        $stock = [];
        $stmt = Database::$Connections['MySQL']->prepare("
            select sh.QuantityOnhand , s.StockItemName , sh.TargetStockLevel,sh.StockItemID
            From stockitemholdings as sh
            join stockitems as s ON sh.StockItemID = s.StockItemID
            where QuantityOnhand < TargetStockLevel
            order by (QuantityOnhand * 100 / TargetStockLevel)
        ");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $stock['notEnough'][] = $row;
        }

        $stmt = Database::$Connections['MySQL']->prepare("
            select sh.QuantityOnhand , s.StockItemName , sh.TargetStockLevel , sh.StockItemID
            From stockitemholdings as sh
            join stockitems as s ON sh.StockItemID = s.StockItemID
            where QuantityOnhand >= TargetStockLevel
            order by (QuantityOnhand * 100 / TargetStockLevel)
            limit 50
        ");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $stock['enough'][] = $row;
        }

        return Pages::view('admin', 'admin/stock/stockcontrol', [
            'stock' => $stock,
            'pageTitle' => 'voorraad',
            'loggedInUser' => AdminAuth::$user
        ]);

    }
    public function getStockEdit(Request $request, $productId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
            Select sh.QuantityOnhand , s.StockItemName , sh.TargetStockLevel , sh.StockItemID,si.ImagePath, SupplierName
            from stockitemholdings sh
            join stockitems s ON sh.StockItemID = s.StockItemID
            left join stockitemimages si on si.StockItemID = sh.StockItemID
            left join suppliers sp on s.SupplierID = sp.SupplierID
            where sh.stockItemID = ?
        ");
        $stmt->bind_param('i', $productId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $stockitem = $row;
        }

        return Pages::view('admin', 'admin/stock/stockeditcontrol', [
            'pageTitle' => 'voorraad',
            'loggedInUser' => AdminAuth::$user,
            'stockitem' => $stockitem
        ]);
    }

    public function updateStock(Request $request, $productId)
    {
        $addStock = $request->input('stockInput');
        $stmt = Database::$Connections['MySQL']->prepare("update stockitemholdings set QuantityOnhand = QuantityOnhand + ? where StockItemID = ? ");
        $stmt->bind_param('si', $addStock, $productId);
        $stmt->execute();

        $uptodateObject = new UptoDateController();
        $uptodateObject->sendToSubscribedUsers($productId, $addStock);

        $this->addNotification('Succes', 'Voorraad is succesvol besteld', 'success');
        header('location:/admin/stock');
    }
}


