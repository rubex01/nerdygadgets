<?php

namespace App\Controllers\Admin\Sales;

use Framework\Pages\Pages;
use Framework\Database\Database;
use Framework\Request\Request;
use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Validation\Validation;

class SalesController
{
    use \App\Controllers\NotificationTrait;

    public function getSales()
    {
        $specialDeals = [];
        $stmt = Database::$Connections['MySQL']->prepare(" 
        SELECT sd.SpecialDealID, StockItemName, sim.ImagePath, sd.EndDate FROM specialdeals sd
        left join stockitems s on s.StockItemID = sd.StockItemID
        left join stockitemimages sim on sim.StockItemID = sd.StockItemID
        group by sd.StockItemID
        order by EndDate
        ");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $specialDeals[] = $row;
        }

        return Pages::View('admin', 'admin/sales/sales', [
                'pageTitle' => 'Speciale Deals',
                'specialDeals' => $specialDeals,
                'loggedInUser' => AdminAuth::$user,
                'sass' => ['admin/sales.css']
            ]);
    }
    
    public function getEdit(Request $request, $specialDealId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
        select SpecialDealID, StockItemName, StartDate, EndDate, DiscountAmount, DiscountPercentage, sd.UnitPrice, sd.StockItemID, DealDescription
        from specialdeals sd
        left join stockitems si on si.StockItemID = sd.StockItemID
        where SpecialDealID = ?
        ");
        $stmt->bind_param('i', $specialDealId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()){
            $specialDeal = $row; 
        }

        return Pages::View('admin', 'admin/sales/edit', [
                    'pageTitle' => 'Speciale Deals',
                    'loggedInUser' => AdminAuth::$user,
                    'specialDeal' => $specialDeal,
                    'sass' => ['admin/sales.css'],
                    'endpoint' => '/admin/endpoint/sales/edit/'.$specialDealId,
                    'js' => ['admin/sales.js']
            ]);
    }

    public function getDelete($SpecialDealID)
    {

        $newDate = date("Y-m-d", strtotime('-1 day'));

        $stmt = Database::$Connections['MySQL']->prepare("update specialdeals set EndDate = ? where SpecialDealID = ?");
        $stmt->bind_param('si', $newDate, $SpecialDealID);
        $stmt->execute();

        $this->addNotification('Success', 'Actie is op inactief gezet', 'success');
        header('location:/admin/sales');
    }

    public function updateSale(Request $request, $specialDealId)
    {
        $validation = new Validation(
            $request->body,
            [
                'StockItemID' => ['required', 'exists:stockitems'],
                'startDate' => ['required', 'date'],
                'endDate' => ['required', 'date'],
                'discountType' => ['required'],
                'dealAmount' => ['required'],
                'dealDescription' => ['required'],
                'unitPrice' => ['required']
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/sales/edit/'.$specialDealId);
            exit();
        }

        $stockItemId = $request->input('StockItemID');
        $endDate = $request->input('endDate');
        $startDate = $request->input('startDate');
        $dealDescription = $request->input('dealDescription');
        $unitPrice = $request->input('unitPrice');
        
        if ($request->input('DiscountType') === 'euro') {
            $discountAmount = $request->input('dealAmount');
            $discountPercentage = 0;
        }
        else {
            $discountAmount = 0;
            $discountPercentage = $request->input('dealAmount');
        }
    
        $stmt = Database::$Connections['MySQL']->prepare("update specialdeals set StockItemID = ?, EndDate = ?, StartDate = ?, DiscountAmount = ?, DiscountPercentage = ?, dealDescription = ?, UnitPrice = ?, LastEditedBy = ?, LastEditedWhen = now()
         where SpecialDealID = ?");
        $stmt->bind_param('issssssis', $stockItemId, $endDate, $startDate, $discountAmount, $discountPercentage, $dealDescription, $unitPrice, AdminAuth::$id, $specialDealId);
        $stmt->execute();

        $this->addNotification('Success', 'Wijzigingen succesvol uitgevoerd', 'success');
        header('location:/admin/sales');
    }

    public function getAdd()
    {
        return Pages::View('admin', 'admin/sales/add', [
            'pageTitle' => 'Speciale Deals',
            'loggedInUser' => AdminAuth::$user,
            'endpoint' => '/admin/endpoint/sales/add',
            'js' => ['admin/sales.js']
        ]);
    }

    public function storeSale(Request $request) {
        $validation = new Validation(
            $request->body,
            [
                'StockItemID' => ['required', 'exists:stockitems', 'unique:specialdeals'],
                'startDate' => ['required', 'date'],
                'endDate' => ['required', 'date'],
                'discountType' => ['required'],
                'dealAmount' => ['required'],
                'dealDescription' => ['required'],
                'unitPrice' => ['required']
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/sales/add');
            exit();
        }

        $stockItemId = $request->input('StockItemID');
        $endDate = $request->input('endDate');
        $startDate = $request->input('startDate');
        $dealDescription = $request->input('dealDescription');
        
        if ($request->input('DiscountType') === 'euro') {
            $discountAmount = $request->input('dealAmount');
            $discountPercentage = 0;
        }
        else {
            $discountAmount = 0;
            $discountPercentage = $request->input('dealAmount');
        }

        $stmt = Database::$Connections['MySQL']->prepare("
        insert into specialdeals 
        (StockItemID, CustomerID, BuyingGroupID, CustomerCategoryID, StockGroupID, DealDescription, StartDate, EndDate, DiscountAmount, DiscountPercentage, UnitPrice, LastEditedBy, LastEditedWhen)
        values (?, null, null, null, null, ?, ?, ?, ?, ?, 0, ?, now())
        ");
        $stmt->bind_param('isssssi', $stockItemId, $dealDescription, $startDate, $endDate, $discountAmount, $discountPercentage, AdminAuth::$id);
        $stmt->execute();

        $this->addNotification('Success', 'Aanbieding is toegevoegd', 'success');
        header('location:/admin/sales');
    }
}
