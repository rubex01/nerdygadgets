<?php

namespace App\Controllers\Admin;

use App\Controllers\NotificationTrait;
use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Framework\Storage\Storage;

class SettingsController
{
    use NotificationTrait;

    public function getSettings()
    {
        return Pages::view('admin', 'admin/settings', [
            'devices'=> $this->getAdminSignedInDevices(),
            'sass' => ['admin/settings.css'],
            'loggedInUser' => AdminAuth::$user,
            'pageTitle' => 'Instellingen',
            'contactEmails' => $this->getContactEmails(),
        ]);
    }

    public function getAdminSignedInDevices()
    {
        $stmt = Database::$Connections['MySQL']->prepare("select token, admin_user_sessions_id, browser, operating_system, device_type, ip_address, valid_until from admin_user_sessions where PersonID = ?");
        $stmt->bind_param('i', AdminAuth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $devices = [];
        while($row = $result->fetch_assoc()) {
            $devices[] = $row;
        }
        return $devices;
    }

    public function updatePhoto(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'photo' => ['file', 'image', 'size:500000'],
            ],
            [
                'photo.file' => 'Er ging iets fout',
                'photo.image' => 'De afbeelding was niet geldig',
                'photo.size:500000' => 'De afbeelding is te groot'
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/settings');
            exit();
        }

        $upload = Storage::upload($request->input('photo'), true, '/storage/people/', false);

        $stmt = Database::$Connections['MySQL']->prepare("update people set Photo = ? where PersonID = ?");
        $stmt->bind_param('si', $upload['fileName'], AdminAuth::$id);
        $stmt->execute();

        $this->addNotification('Success', 'Profielfoto is geupdate', 'success');
        header('location:/admin/settings');
    }

    public function adminSignOutDevice(Request $request, $adminUserSessionId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("delete from admin_user_sessions where admin_user_sessions_id = ? and PersonID = ?");
        $stmt->bind_param('ii', $adminUserSessionId, AdminAuth::$id);
        $stmt->execute();
        $this->addNotification('Succesvol uitgelogd', 'Het apparaat is succesvol uitgelogd van de website' , 'success');
        header('location:/admin/settings');
    }

    public function updatePassword(Request $request)
    {
        $stmt = Database::$Connections['MySQL']->prepare("select HashedPassword from people where PersonID = ?");
        $stmt->bind_param('i', AdminAuth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $databasePassword = $row['HashedPassword'];
        }
        $oldPassword = hash('sha256', $request->input('old_password').getenv('ADMIN_SALT'));
        $inputPassword = hash('sha256', $request->input('password').getenv('ADMIN_SALT'));
        if ($oldPassword !== $databasePassword) {
            $this->addNotification('Verkeerd Wachtwoord', 'Wachtwoord komt niet overeen', 'error');
            header('location:/admin/settings');
            exit();
        }
        $validation = new Validation($request->body, [
            'password' => ['required', 'string', 'confirmation'],
        ],
            [
                'password.required' => 'Wachtwoord is verplicht',
                'password.confirmation' => 'Bevestig uw wachtwoord',
            ]);
        $stmt = Database::$Connections['MySQL']->prepare("update people set HashedPassword = ? where PersonID = ?");
        $stmt->bind_param('si', $inputPassword, AdminAuth::$id);
        $stmt->execute();
        $this->addNotification('Succesvol', 'Wachtwoord is aangepast', 'success');
        header('location:/admin/settings');
    }

    public function updateTheme(Request $request)
    {
        $newThemeSettings = json_encode([
                'main' => $request->input('main'), 
                'tab' => $request->input('tab'), 
                'text' => $request->input('text')
                ]);
        setcookie('theme', $newThemeSettings, time() + (86400 * 30), "/");

        $this->addNotification('Succes', 'Theme is succesvol gewijzigd', 'success');
        header('location:/admin/settings');
    }

    public function resetTheme()
    {
        unset($_COOKIE['theme']); 
        setcookie('theme', null, -1, '/'); 
        $this->addNotification('Succes', 'Originele thema is succesvol hersteld', 'success');
        header('location:/admin/settings');
    }

    public function getContactEmails()
    {
        $stmt = Database::$Connections['MySQL']->prepare(
            "SELECT * from contactemails"
        );
        $stmt->execute();
        $result = $stmt->get_result();
        $emails = [];
        while($row = $result->fetch_assoc()) {
            $emails[] = $row;
        }
        return $emails;
   }

   public function deleteContactEmail($emailId)
   {
       $stmt = Database::$Connections['MySQL']->prepare(
           "DELETE from contactemails where contactemails_id = ?"
       );
       $stmt->bind_param('i', $emailId);
       $stmt->execute();

       $this->addNotification('Succes', 'Email is succesvol verwijderd', 'success');
       header('location:/admin/settings');
   }
  
   public function storeEmail(request $request)
   {
        $validation = new Validation(
            $request->body,
            [
                'email' => ['email', 'unique:contactemails']
            ],
            [
                'email.email' => 'Ongeldig email adres',
                'email.unique:contactemails' => 'Dit email adres is al toegevoegd'
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/settings');
            exit();
        }

        $email = $request->input('email');
        $stmt = Database::$Connections['MySQL']->prepare(
            "INSERT INTO contactemails (email) values (?)"
        );
        $stmt->bind_param('s', $email);
        $stmt->execute();

        $this->addNotification('Succes', 'Email is succesvol toegevoegd', 'success');
        header('location:/admin/settings');
   }
}
