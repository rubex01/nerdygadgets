<?php

namespace App\Controllers\Admin\Users;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;

class EditUserController
{
    use \App\Controllers\NotificationTrait;

    public function getEditUser(Request $request, $personId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
                select IsPermittedToLogon, FullName, LogonName, IsSystemUser, IsEmployee, IsSalesperson, PhoneNumber, EmailAddress, ValidFrom, ValidTo, Photo
                from people
                where PersonID = ?
            ");
        $stmt->bind_param('i', $personId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $userInfo = $row;
        }

        return Pages::view('admin', 'admin/users/edit', [
            'pageTitle' => 'Gebruikers',
            'userInfo' => $userInfo,
            'loggedInUser' => AdminAuth::$user,
            'endpoint' => '/admin/endpoint/users/edit/'.$personId,
            'sass' => ['admin/user-edit.css']
        ]);
    }

    public function editUser(Request $request, $personId)
    {
        $this->validateEditRequest($request, $personId);

        $fullName = $request->input('FullName');
        $loginPermission = $request->input('IsPermittedToLogon');
        $isAdmin = $request->input('IsSystemUser');
        $isEmployee = $request->input('IsEmployee');
        $isSalesPerson = $request->input('IsSalesperson');
        $phone = $request->input('PhoneNumber');
        $email = $request->input('EmailAddress');
        $validFrom = $request->input('ValidFrom');
        $validTo = $request->input('ValidTo');


        $stmt = Database::$Connections['MySQL']->prepare("
                update people set 
                FullName = ?,
                PreferredName = ?,
                SearchName = ?,
                IsPermittedToLogon = ?,
                LogonName = ?,
                IsExternalLogonProvider = 0,
                IsSystemUser = ?,
                IsEmployee = ?,
                IsSalesperson = ?,
                PhoneNumber = ?,
                EmailAddress = ?,
                LastEditedBy = ".AdminAuth::$id.",
                ValidFrom = ?,
                ValidTo = ?
                where PersonID = ?
            ");
        $stmt->bind_param('sssisiiissssi', $fullName, $fullName, $fullName, $loginPermission, $email, $isAdmin, $isEmployee, $isSalesPerson, $phone, $email, $validFrom, $validTo, $personId);
        $stmt->execute();

        $this->addNotification('Succes', 'Wijzigingen zijn succesvol opgeslagen', 'success');
        header('location:/admin/users');
    }

    public function validateEditRequest($request, $personId)
    {
        $validation = new Validation(
            $request->body,
            [
                'FullName' => ['required'],
                'IsPermittedToLogon' => ['required', 'boolean:false'],
                'IsEmployee' => ['required', 'boolean:false'],
                'IsSalesperson' => ['required', 'boolean:false'],
                'PhoneNumber' => ['required', 'phone'],
                'EmailAddress' => ['required', 'email'],
                'ValidFrom' => ['required'],
                'ValidTo' => ['required']
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/users/edit/'.$personId);
            exit();
        }
        return true;
    }
}