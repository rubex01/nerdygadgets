<?php

namespace App\Controllers\Admin\Users;

use Framework\Auth\Auth;
use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Mailgun\Mailgun;

class AddUserController
{
    use \App\Controllers\NotificationTrait;

    public function getAddUser()
    {
        return Pages::view('admin', 'admin/users/add', [
            'pageTitle' => 'Gebruikers',
            'loggedInUser' => AdminAuth::$user,
            'endpoint' => '/admin/endpoint/users/add'
        ]);
    }

    public function addUser(Request $request)
    {
        $this->validateAddRequest($request);

        $fullName = $request->input('FullName');
        $loginPermission = $request->input('IsPermittedToLogon');
        $isAdmin = $request->input('IsSystemUser');
        $isEmployee = $request->input('IsEmployee');
        $isSalesPerson = $request->input('IsSalesperson');
        $phone = $request->input('PhoneNumber');
        $email = $request->input('EmailAddress');
        $validFrom = $request->input('ValidFrom');
        $validTo = $request->input('ValidTo');
        if ($loginPermission == true) {
            $token = md5(uniqid(AdminAuth::$id . rand(), true));
        }
        else {
            $token = null;
        }

        $stmt = Database::$Connections['MySQL']->prepare("
                insert into people 
                (FullName, PreferredName, SearchName, IspermittedToLogon, LogonName, IsExternalLogonProvider, HashedPassword, IsSystemUser, IsEmployee, IsSalesPerson, UserPreferences, PhoneNumber, FaxNumber, EmailAddress, Photo, CustomFields, OtherLanguages, LastEditedBy, ValidFrom, ValidTo, setup_token) values
                (?, ?, ?, ?, ?, 0, '', ?, ?, ?, '', ?, '', ?, 'default.png', '', '', ".AdminAuth::$id.", ?, ?, ?)
            ");
        $stmt->bind_param('sssisiiisssss', $fullName, $fullName, $fullName, $loginPermission, $fullName, $isAdmin, $isEmployee, $isSalesPerson, $phone, $email, $validFrom, $validTo, $token);
        $stmt->execute();

        $emailContent = file_get_contents(__DIR__ . '/../../../../Views/Templating/Pages/mails/admin-setup.page');
        $emailContent = sprintf($emailContent, AdminAuth::$user['FullName'], $token);

        if ($loginPermission == true) {
            $mg = Mailgun::create(getenv('MG_API_KEY'));
            $mg->messages()->send(getenv('MG_DOMAIN'), [
                'from'    => getenv('MG_FROM'),
                'to'      => $email,
                'subject' => 'Wachtwoord instellen',
                'text'    => 'http://localhost/admin/setup/'.$token,
                'html'    => $emailContent
            ]);
        }

        $this->addNotification('Succes', 'Gebruiker is succesvol geregistreerd', 'success');
        header('location:/admin/users');
    }

    public function validateAddRequest($request)
    {
        $validation = new Validation(
            $request->body,
            [
                'FullName' => ['required'],
                'IsPermittedToLogon' => ['required', 'boolean:false'],
                'IsEmployee' => ['required', 'boolean:false'],
                'IsSalesperson' => ['required', 'boolean:false'],
                'PhoneNumber' => ['required', 'phone'],
                'EmailAddress' => ['required', 'email', 'unique:people'],
                'ValidFrom' => ['required'],
                'ValidTo' => ['required']
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/admin/users/add');
            exit();
        }
        return true;
    }
}