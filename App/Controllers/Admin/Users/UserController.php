<?php

namespace App\Controllers\Admin\Users;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;

class UserController
{
    public function getUsers(Request $request)
    {
        $query = $request->input('q');
        $page = $request->input('p');
        $pwCheckEnabled = $request->input('pw-check');

        $queryEnabled = $query !== null && $query !== '' ? true : false;
        $limit1 = $page !== null && $page !== '' ? $page*30 : 0;
        $limit2 = $limit1+30;

        $totalPages = $this->getTotalPagesCount($query, $queryEnabled) / 30;
        $users = $this->getUserData($query, $queryEnabled, $limit1, $limit2);
        $insecureKeys = [];

        if ($pwCheckEnabled == 'enabled') {
            $newUserList = $users;
            $pwList = fopen(__DIR__ . '/../../../../Storage/App/passwordlist.txt', 'r');
            while (($line = fgets($pwList)) !== false && count($newUserList) > 0) {
                $hashToCheck = hash('sha256', substr($line, 0, -1).getenv('ADMIN_SALT'));
                foreach ($newUserList as $key => $user) {
                    if ($user['HashedPassword'] === $hashToCheck) {
                        $insecureKeys[] = $key;
                        unset($newUserList[$key]);
                    }
                }
            }
            fclose($pwList);
        }

        return Pages::view('admin', 'admin/users/users', [
            'pageTitle' => 'Gebruikers',
            'js' => ['admin/pagination.js'],
            'loggedInUser' => AdminAuth::$user,
            'users' => $users,
            'totalPages' => $totalPages,
            'query' => $query,
            'insecureKeys' => $insecureKeys,
            'pageNr' => $page,
            'pwCheckEnabled' => $pwCheckEnabled,
            'sass' => ['admin/users.css']
        ]);
    }

    public function getUserData($query, $queryEnabled, $limit1, $limit2)
    {
        $users = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                select PersonID, IsPermittedToLogon, FullName, LogonName, IsSystemUser, IsEmployee, IsSalesperson, PhoneNumber, EmailAddress, ValidTo, Photo, HashedPassword
                from people p 
                where (FullName like concat('%',?,'%') or EmailAddress like concat('%',?,'%')) or 0 = ?
                group by PersonID
                limit ?, ?
            ");
        $stmt->bind_param('ssiii', $query, $query, $queryEnabled, $limit1, $limit2);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $users[] = $row;
        }
        return $users;
    }

    public function getTotalPagesCount($query, $queryEnabled)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
            select count(PersonID) count from people where (FullName like concat('%',?,'%') or EmailAddress like concat('%',?,'%')) or 0 = ?
        ");
        $stmt->bind_param('ssi', $query, $query, $queryEnabled);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $count = $row['count'];
        }
        return $count;
    }
}