<?php

namespace App\Controllers\Admin\Users;

use Framework\Database\Database;

class DeleteUserController
{
    use \App\Controllers\NotificationTrait;

    public function deleteUser($personId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("update people set ValidTo = now() where PersonID = ?");
        $stmt->bind_param('i', $personId);
        $stmt->execute();

        $this->addNotification('Succes', 'De gebruiker is verwijderd','success');
        header('location:/admin/users');
    }
}