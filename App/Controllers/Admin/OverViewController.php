<?php

namespace App\Controllers\Admin;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;

class OverViewController
{
    public function getOverview()
    {
        $month = date("n");
        $yearQuarter = ceil($month / 3);

        $margins = $this->getRevenueProfit($yearQuarter);
        $marginsLastQ = $this->getRevenueProfit($yearQuarter-1);

        $mostSoldedProducts = $this->getMostSold();

        return Pages::view('admin', 'admin/overview', [
            'pageTitle' => 'Overzicht',
            'loggedInUser' => AdminAuth::$user,
            'sass' => ['admin/overview.css'],
            'revenue' => round($margins['revenue'], 3),
            'profit' => round($margins['profit'], 3),
            'profitLastQ' => round($marginsLastQ['profit'], 3),
            'currentQ' => $yearQuarter,
            'mostSold' => $mostSoldedProducts
        ]);
    }

    public function getRevenueProfit($quarter) : array
    {
        $stmt = Database::$Connections['MySQL']->prepare("
            select sum(RecommendedRetailPrice) revenue, sum(RecommendedRetailPrice) - sum(UnitPrice * (100+TaxRate) / 100) profit 
            from checkout_products cp
            left join checkout co on co.checkout_id = cp.checkout_id
            left join stockitems si on si.StockItemID = cp.StockItemID
            where QUARTER(co.checkout_time) = ?
        ");
        $stmt->bind_param('i', $quarter);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $margins['revenue'] = $row['revenue'];
            $margins['profit'] = $row['profit'];
        }
        return $margins;
    }

    public function getMostSold() : array
    {
        $mostSoldedProducts = [];
        $stmt = Database::$Connections['MySQL']->prepare("
            select cp.StockItemID, ImagePath, StockItemName, (count(cp.StockItemID) + count(amount)) timesbought 
            from checkout_products cp
            left join stockitemimages sim on sim.StockItemID = cp.StockItemID
            left join stockitems si on si.StockItemID = cp.StockItemID
            group by cp.StockItemID
            order by (count(cp.StockItemID) + count(amount)) desc
            limit 6
        ");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $mostSoldedProducts[] = $row;
        }
        return $mostSoldedProducts;
    }
}