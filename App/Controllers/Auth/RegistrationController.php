<?php

namespace App\Controllers\Auth;

use Framework\CustomPackages\AdminAuth\AdminAuth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Mailgun\Mailgun;

class RegistrationController
{
    use \App\Controllers\NotificationTrait;
    use \App\Controllers\RecaptchaTrait;

    public function getAccountRegistration()
    {
        return Pages::view('default', 'auth/register', [
            'sass' => ['register.css']
        ]);
    }

    public function registerAction(Request $request)
    {
        $validation = $this->registerInputValidation($request->body);
        $this->recaptchaValidation($request->input('g-recaptcha-response'), '/registration');

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/registration');
            exit();
        }

        $email = $request->input('email');
        $password = password_hash($request->input('password'), PASSWORD_BCRYPT);
        $phone_number = $request->input('phone_number');
        $first_name = $request->input('first_name');
        $insertion = $request->input('insertion');
        $last_name = $request->input('last_name');
        $birthDate = strtotime($request->input('date_of_birth'));
        $date_of_birth = date('Y-m-d', $birthDate);
        $gender = $request->input('gender');
        $address = $request->input('address');
        $house_number = $request->input('house_number');
        $zipcode = $request->input('zipcode');
        $domicile = $request->input('domicile');
        $province = $request->input('province');
        $newsletter = $request->input('newsletter');
        $agreement = 1;

        $database = Database::$Connections['MySQL'];
        $stmt = $database->prepare('insert into users (email, password, phone_number, first_name, insertion, last_name, date_of_birth, gender, address, house_number, zipcode, domicile, province, newsletter, agreement)
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->bind_param('sssssssssssssss', $email, $password, $phone_number, $first_name, $insertion, $last_name, $date_of_birth, $gender, $address, $house_number, $zipcode, $domicile, $province, $newsletter, $agreement);
        $stmt->execute();

        $emailContent = file_get_contents(__DIR__ . '/../../../Views/Templating/Pages/mails/user-setup.page');
        $emailContent = sprintf($emailContent, $first_name);

        $mg = Mailgun::create(getenv('MG_API_KEY'));
        $mg->messages()->send(getenv('MG_DOMAIN'), [
            'from'    => getenv('MG_FROM'),
            'to'      => $email,
            'subject' => 'Welkom bij NerdyGadgets',
            'text'    => 'Welkom bij NerdyGadgets, Bekijk ons assortiment op http://localhost/products',
            'html'    => $emailContent
        ]);

        header('location:/login');
    }

    public function registerInputValidation(array $input)
    {
        $validation = new Validation(
            $input,
            [
                'email' => ['required', 'email', 'unique:users'],
                'password' => ['required', 'string', 'confirmation'],
                'phone_number' => ['required', 'phone'],
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'date_of_birth' => ['required', 'date'],
                'gender' => ['required'],
                'g-recaptcha-response' => ['required']
            ],
            [
                'email.required' => 'Email is verplicht',
                'email.email' => 'Gebruik een geldig email adres',
                'email.unique:users' => 'Er bestaat al een account met dit email adres',
                'password.required' => 'Wachtwoord is verplicht',
                'password.confirmation' => 'Bevestig uw wachtwoord',
                'phone_number.required' => 'Telefoon nummer is verplicht',
                'phone_number.phone' => 'Telefoon nummer is niet geldig',
                'first_name.required' => 'Voornaam is verplicht',
                'last_name' => 'Achternaam is verplicht',
                'date_of_birth' => 'Geboortedatum is veplicht',
                'gender.required' => 'Geslacht is verplicht',
                'g-recaptcha-response.required' => 'reCAPTCHA is verplicht'
            ]);
        return $validation;
    }
}

