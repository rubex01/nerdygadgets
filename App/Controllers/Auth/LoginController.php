<?php

namespace App\Controllers\Auth;

use Framework\Auth\Auth;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Framework\Database\Database;
use \Mailgun\Mailgun;

class LoginController
{
    use \App\Controllers\NotificationTrait;
    use \App\Controllers\RecaptchaTrait;
    use \App\Controllers\ShoppingCart\UpdateShoppingCartItemsTrait;

    public function getLogin()
    {
        return Pages::view('default', 'auth/login', [
            'sass' => ['login.css']
        ]);
    }

    public function loginAction(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'email' => ['required', 'email', 'exists:users'],
                'password' => ['required', 'string'],
                'g-recaptcha-response' => ['required']
            ],
            [
                'email.required' => 'Email is verplicht',
                'email.email' => 'Gebruik een geldig email adres',
                'email.exists:users' => 'Er bestaat nog geen account met dit email adres',
                'password.required' => 'Wachtwoord is verplicht',
                'g-recaptcha-response.required' => 'reCAPTCHA is verplicht'
            ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/login');
            exit();
        }

        $this->recaptchaValidation($request->input('g-recaptcha-response'), '/login');

        $email = $request->input('email');
        $password = $request->input('password');

        $login = Auth::login(["email" => $email, "password"=> $password ]);

        if ($login === true) {

            $deviceInformation = get_browser(null, true);
            $stmt = Database::$Connections['MySQL']->prepare("select user_sessions_id From user_sessions WHERE user_id = ? and ip_address = ? and browser = ? and operating_system = ?");
            $stmt->bind_param('isss', Auth::$id, $_SERVER['REMOTE_ADDR'], $deviceInformation['browser'], $deviceInformation['platform']);
            $stmt->execute();
            $sameLoginSessions = $stmt->get_result();

            if ($sameLoginSessions->num_rows === 1) {
                $emailContent = file_get_contents(__DIR__ . '/../../../Views/Templating/Pages/mails/new-login.page');
                $emailContent = sprintf($emailContent, $_SERVER['REMOTE_ADDR'], $deviceInformation['browser'], $deviceInformation['platform']);
                $mg = Mailgun::create(getenv('MG_API_KEY'));
                $mg->messages()->send(getenv('MG_DOMAIN'), [
                    'from'    => getenv('MG_FROM'),
                    'to'      => $email,
                    'subject' => 'Nieuwe login',
                    'text'    => 'Er is een nieuwe login via ' . $_SERVER['REMOTE_ADDR'],
                    'html'    => $emailContent
                ]);
            }

            if (isset($_COOKIE['shoppingcart'])) {
                $shoppingCartArray = json_decode($_COOKIE['shoppingcart'], true);
                foreach($shoppingCartArray as $productId => $amount) {
                    $this->updateShoppingCart($productId, $amount, '+', true);
                }
                setcookie('shoppingcart', null, -1, '/');
            }

            $this->addNotification('Ingelogd', 'U bent ingelogd', 'success');

            if ($request->input('checkout') === 'true') {
                header('location:/shopping-cart');
            }
            else {
                header('location:/');
            }
        }
        else {
            $this->addNotification('Niet ingelogd', 'Het wachtwoord komt niet overeen', 'error');
            header('location:/login');
        }
    }
}





