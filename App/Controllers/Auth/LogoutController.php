<?php

namespace App\Controllers\Auth;

use Framework\Auth\Auth;

class LogoutController
{
    public function logout()
    {
        Auth::logout();
        header('location:/');
    }
}