<?php

namespace App\Controllers\Auth;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;
use \Mailgun\Mailgun;

class PasswordResetController
{
    use \App\Controllers\NotificationTrait;

    public function getPasswordResetReq()
    {
        return Pages::view('default', 'auth/passwordreset', []);
    }

    public function requestPasswordReset(Request $request)
    {
        $this->validateRequestPassReset($request);

        $email = $request->input('email');

        $stmt = Database::$Connections['MySQL']->prepare("select user_id, first_name from users where email = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $userId = $row['user_id'];
            $name = $row['first_name'];
        }

        $token = bin2hex(random_bytes(70));
        $validTo = date('Y-m-d H:i:s', strtotime("+15 minutes"));

        $stmt = Database::$Connections['MySQL']->prepare("insert into password_reset_links (user_id, link, valid_to) values (?, ?, ?)");
        $stmt->bind_param('iss', $userId, $token, $validTo);
        $stmt->execute();

        $emailContent = file_get_contents(__DIR__ . '/../../../Views/Templating/Pages/mails/password-reset.page');
        $emailContent = sprintf($emailContent, $name, $token);

        $mg = Mailgun::create(getenv('MG_API_KEY'));
        $mg->messages()->send(getenv('MG_DOMAIN'), [
            'from'    => getenv('MG_FROM'),
            'to'      => $email,
            'subject' => 'Wachtwoord herstellen',
            'text'    => 'http://localhost/password-reset/'.$token,
            'html'    => $emailContent
        ]);

        $this->addNotification('Succes', 'U heeft een wachtwoord herstel link in uw mailbox ontvangen', 'success');
        header('location:/login');
    }

    protected function validateRequestPassReset($request)
    {
        $validation = new Validation(
            $request->body,
            [
                'email' => ['required', 'email', 'exists:users'],
                'g-recaptcha-response' => ['required']
            ],
            [
                'email.required' => 'Email is verplicht',
                'email.email' => 'Gebruik een geldig email adres',
                'email.exists:users' => 'Er bestaat nog geen account met dit email adres',
                'g-recaptcha-response.required' => 'reCAPTCHA is verplicht'
            ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/password-reset');
            exit();
        }
    }

    public function getResetPassword(Request $request, $resetToken)
    {
        $stmt = Database::$Connections['MySQL']->prepare("select password_reset_id from password_reset_links where link = ? and valid_to > now()");
        $stmt->bind_param('s', $resetToken);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $this->addNotification('Deze link is niet geldig', 'Deze link is niet geldig, probeer opnieuw een herstel link aan te vragen.', 'error');
            header('location:/password-reset');
            return;
        }

        return Pages::view('default', 'auth/passwordresetaction', [
            'resetToken' => $resetToken
        ]);
    }

    public function passwordResetAction(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'password' => ['required', 'string', 'confirmation'],
                'resetToken' => ['required']
            ],
            [
                'password.required' => 'Wachtwoord is verplicht',
                'password.confirmation' => 'De gegeven wachtwoorden komen niet overeen',
            ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/password-reset/'.$request->input('resetToken'));
            exit();
        }

        $password = password_hash($request->input('password'), PASSWORD_BCRYPT);
        $resetToken = $request->input('resetToken');

        $stmt = Database::$Connections['MySQL']->prepare("update users set password = ? where user_id = (select user_id from password_reset_links where link = ?)");
        $stmt->bind_param('ss', $password, $resetToken);
        $stmt->execute();

        $stmt = Database::$Connections['MySQL']->prepare("delete from password_reset_links where link = ?");
        $stmt->bind_param('s', $resetToken);
        $stmt->execute();

        $this->addNotification('Succes', 'Uw wachtwoord is succesvol gewijzigd', 'error');
        header('location:/login');
    }
}