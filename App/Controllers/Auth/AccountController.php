<?php

namespace App\Controllers\Auth;

use Framework\Pages\Pages;
use Framework\Auth\Auth;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Framework\Database\Database;

class AccountController
{
    use \App\Controllers\NotificationTrait;

    public function getAccount()
    {
        $user = Auth::$user;
        $devices = $this->getSignedInDevices();

        return Pages::view('default', 'account', [
            'user' => $user,
            'devices' => $devices,
            'sass' => ['account.css']
        ]);
    }

    public function deleteAccount(Request $request)
    {
        if ($request->input('email') !== Auth::$user['email']) {
            $this->addNotification('Invoer niet geldig', 'Dit is niet uw email adres', 'error');
            header('location:/my-account');
            exit();
        }

        $userId = Auth::$id;
        $empty = '';

        Auth::logout();

        $stmt = Database::$Connections['MySQL']->prepare("update users set email = ?, password = ? where user_id = ?");
        $stmt->bind_param('ssi', $empty, $empty, $userId);
        $stmt->execute();

        $this->addNotification('Account is verwijderd', 'Uw account is succesvol verwijderd', 'success');
        header('location:/');
    }

    public function updateAccountData(Request $request)
    {
        $this->validateAccountDataUpdate($request);

        $email = $request->input('email');
        $phone = $request->input('phone_number');
        $gender = $request->input('gender');
        $firstName = $request->input('first_name');
        $insertion = $request->input('insertion');
        $lastName = $request->input('last_name');
        $dateOfBirth = $request->input('date_of_birth');
        $newsletter = $request->input('newsletter');

        $stmt = Database::$Connections['MySQL']->prepare("
                update users set 
                email = ?, 
                phone_number = ?, 
                gender = ?, 
                first_name = ?,
                insertion = ?,
                last_name = ?,
                date_of_birth = ?,
                newsletter = ?
                where user_id = ?
            ");
        $stmt->bind_param('sssssssii', $email, $phone, $gender, $firstName, $insertion, $lastName, $dateOfBirth, $newsletter, Auth::$id);
        $stmt->execute();

        $this->addNotification('Succes', 'Wijzingen zijn opgeslagen', 'success');
        header('location:/my-account');
    }

    public function updateAddressData(Request $request)
    {
        $this->validateAddressDataUpdate($request);

        $address = $request->input('address');
        $houseNumber = $request->input('house_number');
        $zipcode = $request->input('zipcode');
        $domicile = $request->input('domicile');
        $province = $request->input('province');

        $stmt = Database::$Connections['MySQL']->prepare("
            update users set 
            address = ?, 
            house_number = ?, 
            zipcode = ?, 
            domicile = ?,
            province = ?
            where user_id = ?
        ");
        $stmt->bind_param('sssssi', $address, $houseNumber, $zipcode, $domicile, $province, Auth::$id);
        $stmt->execute();

        $this->addNotification('Succes', 'Wijzingen zijn opgeslagen', 'success');
        header('location:/my-account');
    }

    public function updatePassword(Request $request)
    {
        $this->validatePasswordUpdate($request);

        $oldPassword = $request->input('old_password');
        $password = $request->input('password');

        $stmt = Database::$Connections['MySQL']->prepare("select password from users where user_id = ?");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $dbPassword = $stmt->get_result()->fetch_assoc()['password'];

        if (password_verify($oldPassword, $dbPassword)) {
            $password = password_hash($password, PASSWORD_BCRYPT);
            $stmt = Database::$Connections['MySQL']->prepare("update users set password = ? where user_id = ?");
            $stmt->bind_param('si', $password, Auth::$id);
            $stmt->execute();
            $this->addNotification('Succes', 'Wachtwoord is gewijzigd', 'success');
            header('location:/my-account');
            exit();
        }

        $this->addNotification('Invoer is niet geldig', 'Uw oude wachtwoord komt niet overeen', 'error');
        header('location:/my-account');
    }

    public function validatePasswordUpdate(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'old_password' => ['required'],
                'password' => ['required', 'string', 'confirmation']
            ],
            [
                'old_password.required' => 'Oude wachtwoord is verplicht',
                'password.required' => 'Wachtwoord is verplicht',
                'password.confirmation' => 'Uw wachtwoorden komen niet overeen'
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/my-account');
            exit();
        }
    }

    public function validateAddressDataUpdate(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'address' => ['required', 'string'],
                'house_number' => ['required', 'string'],
                'zipcode' => ['required', 'string'],
                'domicile' => ['required', 'string'],
                'province' => ['required', 'string'],
            ],
            [
                'address.required' => 'Adres is verplicht',
                'house_number.required' => 'Huisnummer is verplicht',
                'zipcode.required' => 'Postcode is verplicht',
                'domicile.required' => 'Woonplaats is verplicht',
                'province.required' => 'Provincie is verplicht',
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/my-account');
            exit();
        }
    }

    public function validateAccountDataUpdate(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'email' => ['required', 'email'],
                'phone_number' => ['required', 'phone'],
                'first_name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'date_of_birth' => ['required', 'date'],
            ],
            [
                'email.required' => 'Email is verplicht',
                'email.email' => 'Gebruik een geldig email adres',
                'phone_number.phone' => 'Gebruik een geldig telefoon nummer',
                'first_name.required' => 'Voornaam is verplicht',
                'last_name.required' => 'Achternaam is verplicht',
                'date_of_birth.date' => 'Gebruik een geldige geboortedatum',
                'date_of_birth.required' => 'Geboortedatum is verplicht'
            ]
        );

        $email = $request->input('email');

        $stmt = Database::$Connections['MySQL']->prepare("select user_id from users where email = ? and user_id <> ?");
        $stmt->bind_param('si', $email, Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();

        if (count($validation->validationErrors) > 0 || $result->num_rows !== 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            if ($result->num_rows !== 0) {
                $this->addNotification('Invoer is niet geldig', 'Dit email adres is al in gebruik', 'error');
            }
            header('location:/my-account');
            exit();
        }
    }

    public function getSignedInDevices()
    {
        $stmt = Database::$Connections['MySQL']->prepare("select token, user_sessions_id, browser, operating_system, device_type, ip_address from user_sessions where user_id = ?");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $devices = [];
        while($row = $result->fetch_assoc()) {
            $devices[] = $row;
        }
        return $devices;
    }

    public function signedOutDevice(Request $request, $userSessionId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("delete from user_sessions where user_sessions_id = ? and user_id = ?");
        $stmt->bind_param('ii', $userSessionId, Auth::$id);
        $stmt->execute();
        $this->addNotification('Succesvol uitgelogd', 'Het apparaat is succesvol uitgelogd van de website' , 'success');
        header('location:/my-account');
    }
}