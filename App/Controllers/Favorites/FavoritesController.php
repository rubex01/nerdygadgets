<?php

namespace App\Controllers\Favorites;

use Framework\Auth\Auth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Validation\Validation;

class FavoritesController
{
    use \App\Controllers\NotificationTrait;

    public function getFavorites()
    {
        $favorites = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                Select f.StockItemID, avg(stars) stars, RecommendedRetailPrice, favorite_id, ImagePath, StockItemName from favorites f 
                left join stockitems si on f.StockItemID = si.StockItemID 
                left join stockitemimages sim on f.StockItemId = sim.StockItemId 
                left join reviews r on r.StockItemID = f.StockItemID
                where f.user_id = ? 
                group by f.StockItemID
                order by StockItemName desc
            ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $favorites[] = $row;
        }

        return Pages::view('default', 'favorites', [
            'sass' => ['favorites.css'],
            'js' => ['favoritesupdate.js'],
            'products' => $favorites
        ]);
    }

    public function deleteFavorite($productId) {
        $this->validateDeleteRequest($productId);

        $stmt = Database::$Connections['MySQL']->prepare("delete from favorites where user_id = ? and StockItemID = ?");
        $stmt->bind_param('ii', Auth::$id, $productId);
        $stmt->execute();

        header('location:/favorites');
    }

    public function validateDeleteRequest(int $productId)
    {
        $validation = new Validation(
            [
                'StockItemID' => $productId
            ],
            [
                'StockItemID' => ['required', 'exists:stockitems'],
            ],
            [
                'StockItemID.exists:stockitems' => 'Product niet gevonden in verlanglijstje',
            ]
        );

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/favorites');
            exit();
        }

        return;
    }
}