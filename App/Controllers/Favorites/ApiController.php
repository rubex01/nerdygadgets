<?php

namespace App\Controllers\Favorites;

use Framework\Database\Database;
use Framework\Auth\Auth;
use Framework\Request\Request;
use Framework\Validation\Validation;

class ApiController
{
    public function __construct()
    {
        header('Content-type: application/json');
    }

    public function getFavorites()
    {
        $favorites = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                Select f.StockItemID, ImagePath, StockItemName from favorites f 
                left join stockitems si on f.StockItemID = si.StockItemID 
                left join stockitemimages sim on f.StockItemId = sim.StockItemId 
                where f.user_id = ? 
                group by f.StockItemID
                order by f.favorite_id desc
            ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $favorites[] = $row;
        }
        echo json_encode($favorites);
    }

    public function updateFavorites(Request $request)
    {
        $validateUpdateData = $this->validateUpdateData($request);
        $productId = $request->input('StockItemID');

        if ($request->input('favorite') == true) {
            $stmt = Database::$Connections['MySQL']->prepare("insert into favorites (user_id, StockItemID) values (?, ?)");
            $stmt->bind_param('ii', Auth::$id, $productId);
            $stmt->execute();
        }
        else {
            $stmt = Database::$Connections['MySQL']->prepare("delete from favorites where user_id = ? and StockItemID = ?");
            $stmt->bind_param('ii', Auth::$id, $productId);
            $stmt->execute();
        }

        echo json_encode(['success' => true]);
    }

    public function validateUpdateData(Request $request)
    {
        $validation = new Validation(
            $request->body,
            [
                'StockItemID' => ['required', 'exists:stockitems'],
                'favorite' => ['required', 'boolean:false'],
            ],
            [
                'productId.exists:stockitems' => 'Er ging iets fout',
                'favorite.boolean:false' => 'Er ging iets fout'
            ]
        );

        if (count($validation->validationErrors) > 0) {
            echo json_encode($validation->validationErrors);
            exit();
        }

        return;
    }
}