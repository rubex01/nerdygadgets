<?php

namespace App\Controllers;

use Framework\Database\Database;
use Framework\Request\Request;

class TemperatureController
{
    public function __construct()
    {
        header('Content-type: application/json');
    }

    public function getLiveTemperatures()
    {
        $temperatures = [];
        $stmt = Database::$Connections['MySQL']->prepare("select Temperature from coldroomtemperatures limit 5");
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $temperatures[] = $row['Temperature'];
        }
        echo json_encode($temperatures);
    }

    public function updateTemperature(Request $request)
    {
        if ($request->input('token') != getenv('TEMP_TOKEN')) {
            echo json_encode(['error' => 'Invalid token']);
            exit();
        }

        $recordedTemps = $request->input('temperatures');

        $types = '';
        $params = [];
        $values = '';

        foreach ($recordedTemps as $temperature) {
            $types .= 'iss';
            $params[] = $temperature['ColdRoomSensorNumber'];
            $params[] = $temperature['RecordedWhen'];
            $params[] = $temperature['Temperature'];
            $values .= '(?, ?, ?, now(), "9999-12-31 23:59:59"),';
        }

        $values = substr($values, 0, -1);

        $stmt = Database::$Connections['MySQL']->prepare("CALL reset_coldroom_table()");
        $stmt->execute();

        $stmt = Database::$Connections['MySQL']->prepare("
                insert into coldroomtemperatures
                (ColdRoomSensorNumber, RecordedWhen, Temperature, ValidFrom, ValidTo)
                values $values
            ");
        $stmt->bind_param($types, ...$params);
        $stmt->execute();

        echo json_encode(['success' => 'Temperatures have been updated']);
    }
}