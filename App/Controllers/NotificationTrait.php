<?php

namespace App\Controllers;

trait NotificationTrait
{
    public function addNotification(string $notificationTitle, string $notificationText, string $notificationType) : bool
    {
        $_SESSION['notifications'][] = [
            'notificationTitle' => $notificationTitle,
            'notificationText' => $notificationText,
            'notificationType' => $notificationType
        ];
        return true;
    }
}