<?php

namespace App\Controllers\Retour;

use Framework\Database\Database;
use Framework\Auth\Auth;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Validation\Validation;

class RetourController
{
    use \App\Controllers\NotificationTrait;

    public function getRetour()
    {
        $retourProducts = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                select ch.checkout_product_id, checkout_time, StockItemName, ch.StockItemID, ImagePath from checkout c 
                left join checkout_products ch on c.checkout_id = ch.checkout_id
                left join stockitems s on ch.StockItemID = s.StockItemID
                left join stockitemimages st on s.StockItemID = st.StockItemID
                where c.user_id = ? 
                and (ch.amount > (select sum(amount) from retour where checkout_product_id = ch.checkout_product_id) or 
                (select sum(amount) from retour where checkout_product_id = ch.checkout_product_id) IS NULL)
                and ImagePath = (select ImagePath from stockitemimages where StockItemID = ch.StockItemID limit 1)
                and c.canceled = 0
                and checkout_time > DATE(NOW() - INTERVAL 3 MONTH)
            ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $retourProducts[] = $row;
        }

        $readyForRetour = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                select StockItemName, ch.StockItemID, ImagePath, r.amount, retour_time from retour r
                left join checkout_products ch on r.checkout_product_id = ch.checkout_product_id
                left join checkout c on c.checkout_id = ch.checkout_id
                left join stockitems s on ch.StockItemID = s.StockItemID
                left join stockitemimages st on s.StockItemID = st.StockItemID
                where c.user_id = ?
                and c.canceled = 0
                and ImagePath = (select ImagePath from stockitemimages where StockItemID = ch.StockItemID limit 1)
                group by retour_id
            ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $readyForRetour[] = $row;
        }

        return Pages::view('default', 'retour', [
            'retourProducts' => $retourProducts,
            'readyForRetour' => $readyForRetour,
            'sass' => ['retour.css']
        ]);
    }
    
    public function getRetourProduct(Request $request, $retourId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
                select ch.checkout_product_id, StockItemName, checkout_time, ImagePath, (ch.amount - sum(r.amount)) amount, ch.amount originalAmount from checkout c 
                left join checkout_products ch on c.checkout_id = ch.checkout_id
                left join stockitems s on ch.StockItemID = s.StockItemID 
                left join stockitemimages st on s.StockItemID = st.StockItemID
                left join retour r on r.checkout_product_id = ch.checkout_product_id
                where c.user_id = ?
                and ImagePath = (select ImagePath from stockitemimages where StockItemID = ch.StockItemID limit 1)
                and ch.checkout_product_id = ?
                and c.canceled = 0
                and checkout_time > DATE(NOW() - INTERVAL 3 MONTH)
            ");
        $stmt->bind_param('ii', Auth::$id, $retourId);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $retourProduct = $row;
        }

        return Pages::view('default', 'retourproduct', [
            'retourProduct' => $retourProduct,
            'sass' => ['retourproduct.css']
        ]);
    }

    public function retourProduct(Request $request, $checkoutProductId)
    {
        $this->validateRetourProduct($request, $checkoutProductId);

        $amount = ($request->input('returnAmount') != null || $request->input('returnAmount') != '') ? $request->input('returnAmount') : 1;
        $reason = $request->input('reason');
        $retourTime = date('Y-m-d H:m:s');
        
        $stmt = Database::$Connections['MySQL']->prepare("insert into retour (checkout_product_id, amount, reason, retour_time) values (?, ?, ?, ?)");
        $stmt->bind_param('iiss', $checkoutProductId, $amount, $reason, $retourTime);
        $stmt->execute();

        $this->addNotification('Succes', 'Product is succesvol voor retour aangemeld', 'success');
        header('location:/retour');
    }

    protected function validateRetourProduct($request, $checkoutProductId)
    {
        $validation = new Validation(
            $request->body,
            [
                'reason' => ['required', 'string'],
            ],
            [
                'reason.required' => 'Reden is verplicht',
            ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/retour/'.$checkoutProductId);
            exit();
        }

        $stmt = Database::$Connections['MySQL']->prepare("
                select (ch.amount - sum(r.amount)) amount from checkout c 
                left join checkout_products ch on c.checkout_id = ch.checkout_id
                left join retour r on r.checkout_product_id = ch.checkout_product_id
                where c.user_id = ?
                and c.canceled = 0
                and ch.checkout_product_id = ?
            ");
        $stmt->bind_param('ii', Auth::$id, $checkoutProductId);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $amount = $row['amount'];
        }

        if (($amount > 1 && $amount !== null) && ($request->input('returnAmount') == null || $request->input('returnAmount') == '')) {
            $this->addNotification('Invoer is niet geldig', 'U moet aangeven hoeveel producten u wilt retourneren', 'error');
            header('location:/retour/'.$checkoutProductId);
            exit();
        }

        return true;
    }
}