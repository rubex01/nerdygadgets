<?php

namespace App\Controllers\Products;

use Framework\Database\Database;

trait RelatedProductsTrait
{
    public function getRelatedProducts(array $product, $returnAmmount = 6) : array
    {
        $tags = json_decode($product['Tags']);
        $supplier = $product['SupplierID'];

        $firstQuery = '';
        $secondQuery = '';
        $thirdQuery = 'SupplierID = ? ';
        $fourtQuery = '';
        $types = 'i';
        $parameters = [$supplier];

        foreach ($tags as $key => $tag) {
            $firstQuery .= ($key === 0) ? ' OR ' : '';
            $firstQuery .= ' Tags LIKE ? OR (select Tags from specialdeals where StockItemID = si.StockItemID) like ? ';
            $parameters[] = "'%".$tag."%'";
            $parameters[] = "'%".$tag."%'";
            $types .= 'ss';
            if ($key !== count($tags)-1) {
                $firstQuery .= ' OR ';
            }
        }

        $parameters[] = $product['StockItemID'];
        $types .= 'i';

        foreach ($tags as $key => $tag) {
            $secondQuery .= ($key === 0) ? ' when ( ' : '';
            $secondQuery .= ' (select Tags from specialdeals where StockItemID = si.StockItemID) like ? ';
            $parameters[] = "'%".$tag."%'";
            $types .= 's';
            if ($key !== count($tags)-1) {
                $secondQuery .= ' OR ';
            }
            else {
                $secondQuery .= ') then 1 ';
            }
        }

        $parameters[] = $supplier;
        $types .= 'i';

        foreach ($tags as $key => $tag) {
            $thirdQuery .= ($key === 0) ? ' AND ' : '';
            $thirdQuery .= ' Tags like ? ';
            $parameters[] = "'%".$tag."%'";
            $types .= 's';
            if ($key !== count($tags)-1) {
                $thirdQuery .= ' OR ';
            }
        }

        $parameters[] = $supplier;
        $types .= 'i';

        foreach ($tags as $key => $tag) {
            $fourtQuery .= ' when Tags LIKE ? then '.($key+4).' ';
            $parameters[] = "'%".$tag."%'";
            $types .= 's';
        }

        $relatedProducts = [];
        $stmt = Database::$Connections['MySQL']->prepare("
            SELECT 
                si.StockItemID, Video, QuantityOnHand, StockItemName, description, Brand, Size, RecommendedRetailPrice,
                TypicalWeightPerUnit, MarketingComments, CustomFields, Tags, ColorName, Video, ImagePath, SpecialDealID,
                DealDescription, DiscountAmount, DiscountPercentage
                FROM stockitems si
            left join stockitemholdings sh on sh.StockItemID = si.StockItemID
            left join specialdeals sd on sd.StockItemID = si.StockItemID
            left join colors co on co.ColorID = si.ColorID
            left join stockitemimages sim on sim.StockItemId = si.StockItemId
            WHERE
            (
                SupplierID = ? 
                ".$firstQuery."
            ) and
            QuantityOnHand > 0 and
            si.StockItemID <> ?
            group by si.StockItemID
            order by case
                ".$secondQuery."
                when (".$thirdQuery.") then 2
                when SupplierID = ? then 3
                ".$fourtQuery."
                else 6
            end
            limit $returnAmmount
        ");
        $stmt->bind_param($types, ...$parameters);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $relatedProducts[] = $row;
        }
        return $relatedProducts;
    }
}