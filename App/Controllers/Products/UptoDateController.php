<?php

namespace App\Controllers\Products;

use Framework\Database\Database;
use Framework\Request\Request;
use Framework\Auth\Auth;
use Mailgun\Mailgun;
use Mailgun\Message\MessageBuilder;

class UptoDateController
{
    use \App\Controllers\NotificationTrait;

    public function setUptodateTrigger(Request $request, $productId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("insert into uptodate (user_id, StockItemID) values (?, ?)");
        $stmt->bind_param('ii', Auth::$id, $productId);
        $stmt->execute();

        $this->addNotification('Succes', 'Wanneer er weer voorraad is wordt u op de hoogte gesteld via de mail', 'success');
        header('location:/product/'.$productId);
    }

    public function sendToSubscribedUsers($productId, $amount)
    {
        $allRecipients = [];
        $stmt = Database::$Connections['MySQL']->prepare("select email, StockItemName from uptodate utd left join users u on u.user_id = utd.user_id left join stockitems si on si.StockItemId = utd.StockItemId where utd.StockItemID = ? group by utd.user_id");
        $stmt->bind_param('i', $productId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $allRecipients[] = $row['email'];
            $productName = $row['StockItemName'];
        }

        if (count($allRecipients) > 0) {
            $emailContent = file_get_contents(__DIR__ . '/../../../Views/Templating/Pages/mails/new-in-stock.page');
            $emailContent = sprintf($emailContent, $productName, $amount, $productId);

            $builder = new MessageBuilder();
            $builder->setFromAddress(getenv('MG_FROM'));
            $builder->setSubject('Er is weer voorraad beschikbaar');
            $builder->setTextBody('Er is weer voorraad beschikbaar voor een product waarvan u op de hoogte wil blijven. Bekijk http://localhost/product/'.$productId);
            $builder->setHtmlBody($emailContent);

            foreach ($allRecipients as $recipient) {
                $builder->addToRecipient($recipient);
            }

            $mg = Mailgun::create(getenv('MG_API_KEY'));
            $mg->messages()->send(getenv('MG_DOMAIN'), $builder->getMessage());
        }

        $stmt = Database::$Connections['MySQL']->prepare("delete from uptodate where StockItemID = ?");
        $stmt->bind_param('i', $productId);
        $stmt->execute();

        return true;
    }
}