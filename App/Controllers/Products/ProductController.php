<?php

namespace App\Controllers\Products;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Framework\Auth\Auth;
use Framework\Validation\Validation;

class ProductController
{
    use \App\Controllers\NotificationTrait;
    use RelatedProductsTrait;

    public function getProduct(Request $request, int $productId)
    {
        if (Auth::check()) {
            $this->registerInterest($productId);
        }

        $product = $this->getProductInfo($productId);

        $images = [];
        foreach ($product as $productImage) {
            $images[] = $productImage['ImagePath'];
        }

        $product = $product[0];

        $relatedProducts = $this->getRelatedProducts($product);

        $reviews = $this->getReviews($productId);

        $price = explode('.', $product['RecommendedRetailPrice']);

        $specifications = json_decode($product['CustomFields']);

        return Pages::view('default', 'product', [
            'sass' => ['product.css'],
            'js' => ['product.js', 'addToShoppingcart.js'],
            'product' => $product,
            'images' => $images,
            'price' => $price,
            'specifications' => $specifications,
            'reviews' => $reviews,
            'reviewWritePermission' => $this->checkReviewAllowance($productId),
            'relatedProducts' => $relatedProducts
        ]);
    }

    public function getReviews(int $productId) : array
    {
        $reviews = [];
        $stmt = Database::$Connections['MySQL']->prepare("select text, stars from reviews where StockItemID = ? order by review_id limit 10");
        $stmt->bind_param('i', $productId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $reviews[] = $row;
        }
        return $reviews;
    }

    public function getProductInfo(int $productId) : array
    {
        $userId = Auth::check() ? Auth::$id : 0;
        $product = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                select avg(stars) over() stars, sg.StockGroupID, deliverytime, si.StockItemID, Video, DiscountAmount, DiscountPercentage, QuantityOnHand, StockItemName, description, SupplierName, si.SupplierID, Brand, Size, RecommendedRetailPrice, TypicalWeightPerUnit, MarketingComments, CustomFields, Tags, ColorName, Video, sim.ImagePath, favorite_id, IsChillerStock
                from stockitems si 
                left join colors co on co.ColorID = si.ColorID 
                left join suppliers su on su.SupplierID = si.SupplierID 
                left join stockitemimages sim on sim.StockItemId = si.StockItemId
                left join stockitemholdings sih on sih.StockItemID = si.StockItemID
                left join stockitemstockgroups sg on sg.StockItemID = si.StockItemID
                left join specialdeals sd on sd.StockItemID = si.StockItemID and EndDate >= CURRENT_DATE and StartDate <= CURRENT_DATE
                left join reviews r on r.StockItemID = si.StockItemID
                left join favorites f on f.StockItemID = si.StockItemID and f.user_id = ?
                where si.StockItemID = ? and si.ValidTo >= NOW()
                group by sim.ImagePath
            ");
        $stmt->bind_param('ii', $userId, $productId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $product[] = $row;
        }
        return $product;
    }

    public function registerInterest(int $productId) : bool
    {
        $stmt = Database::$Connections['MySQL']->prepare("insert into userstockiteminterest (user_id, StockItemID) values (?, ?)");
        $stmt->bind_param('ss', Auth::$id, $productId);
        $stmt->execute();
        return true;
    }

    public function postReview(Request $request, int $productId) : void
    {
        $validation = new Validation(
            $request->body,
            [
                'text' => ['required', 'string'],
                'stars' => ['required'],
            ],
            [
                'text.required' => 'Review text veld is verplicht',
                'stars.required' => 'Review sterren veld is verplicht'
        ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/product/'.$productId);
            exit();
        }

        if ($request->input('stars') < 1 || $request->input('stars') > 5) {
            $this->addNotification('Invoer is niet geldig', 'Gebruik voor de rating een cijfer van 1 tot en met 5', 'error');
            header('location:/product/'.$productId);
            exit();
        }

        if ($this->checkReviewAllowance($productId) === false) {
            header('location:/product/'.$productId);
            exit();
        }

        $userId = Auth::$id;
        $text = $request->input('text');
        $stars = $request->input('stars');
        $stmt = Database::$Connections['MySQL']->prepare("insert into reviews (user_id, StockItemID, text, stars) values (?, ?, ?, ?)");
        $stmt->bind_param('ssss', $userId, $productId, $text, $stars);
        $stmt->execute();

        header('location:/product/'.$productId);
    }

    public function checkReviewAllowance(int $productId) : bool
    {
        if (!Auth::check()) {
            return false;
        }

        $stmt = Database::$Connections['MySQL']->prepare("select review_id from reviews where StockItemID = ? and user_id = ?");
        $stmt->bind_param('ii', $productId, Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows !== 0) {
            return false;
        }

        $stmt = Database::$Connections['MySQL']->prepare("select StockItemID from checkout left join checkout_products on checkout.checkout_id = checkout_products.checkout_id where user_id = ? and StockItemID = ?");
        $stmt->bind_param('ii', Auth::$id, $productId);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows == 0) {
            return false;
        }

        return true;
    }
}