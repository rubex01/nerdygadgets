<?php

namespace App\Controllers\Products;

use Framework\Database\Database;
use Framework\Request\Request;
use Framework\Validation\Validation;

class ApiController
{
    public function __construct()
    {
        header('Content-type: application/json');
    }

    public function getProductInfo(Request $request, $productId)
    {
        
        $validate =  new Validation(
            ['StockItemID' => $productId],
            [
                'StockItemID' => ['required', 'exists:stockitems']
            ]
        );

        if(count($validate->validationErrors) > 0) {
            http_response_code(404);
            exit();
        }

        $stmt = Database::$Connections['MySQL']->prepare("SELECT StockItemName from stockitems where StockItemID = ?");
        $stmt->bind_param('i', $productId);
        $stmt->execute();
        $result = $stmt->get_result();
        while($row = $result->fetch_assoc()) {
            $product = $row;
        }

        echo json_encode($product);
    }
}