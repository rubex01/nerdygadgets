<?php

namespace App\Controllers\Products;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;

class ProductsController
{
    public function getProductspage(Request $request)
    {
        // Retrieve categories from database.
        $categories = $this->getCategories();

        // Retrieve category and category name.
        $category = $request->input('cat');
        if ($category == '') $category = null;

        $categoryName = "";

        if ($category !== null) {
            foreach($categories as $c) {
                if ($category == $c['StockGroupID']) {
                    $categoryName = $c['StockGroupName'];
                }
            }
        }

        // Retrieve query if specified.
        $query = $request->input('query');

        // Retrieve min and max prices
        $minPrice = $request->input('min');
        $maxPrice = $request->input('max');

        // Retrieve pagination params.
        $perRequest = $request->input('per');
        $productsPerPage =
            ($perRequest !== '' && ($perRequest == 25
                    || $perRequest == 50
                    || $perRequest == 100)) ? $perRequest : 25;

        // Query parameters
        $queryEnabled = $query !== null;
        $categoryEnabled = $category !== null;
        $priceEnabled = $minPrice !== null || $maxPrice !== null;

        if ($minPrice === '') $minPrice = 0;
        if ($maxPrice === '') $maxPrice = 3000;

        $totalPageCount = ceil( $this->getTotalPageCount($category, $query, $minPrice, $maxPrice, $categoryEnabled, $queryEnabled, $priceEnabled) / $productsPerPage);

        $pageRequest = $request->input('p');
        $currentPage = ($pageRequest != '' && $pageRequest >= 1 && $pageRequest <= $totalPageCount) ? $request->input('p') : 1;

        // Retrieve products from database.
        $products = $this->getProducts($category, $query, $currentPage, $minPrice, $maxPrice, $productsPerPage, $categoryEnabled, $queryEnabled, $priceEnabled);

        return Pages::view('products', 'products', [
            'sass' => ['products.css'],
            'js' => ['addToShoppingcart.js'],
            'currentPage' => $currentPage,
            'productsPerPage' => $productsPerPage,
            'totalPageCount' => $totalPageCount,
            'cat' => $category,
            'catName' => $categoryName,
            'query' => $query,
            'min' => $minPrice,
            'max' => $maxPrice,
            'categories' => $categories,
            'products' => $products
        ]);
    }

    public function getProducts($category, $query, $currentPage, $minPrice, $maxPrice, $productsPerPage, $categoryEnabled, $queryEnabled, $priceEnabled)
    {
        $products = [];

        // Determine starting index for query.
        $startAt = $productsPerPage * ($currentPage - 1);

        $stmt = Database::$Connections['MySQL']->prepare("
                select 
                    sg.StockGroupID, si.StockItemID, StockItemName, RecommendedRetailPrice, MarketingComments, ImagePath, 
                    avg(stars) as AverageStars, DiscountPercentage, DiscountAmount
                from stockitems si
                left join stockitemstockgroups sg on si.StockItemID = sg.StockItemID
                left join reviews r on si.StockItemID = r.StockItemID
                left join stockitemimages sim on si.StockItemID = sim.StockItemID
                left join stockitemholdings sih on si.StockItemID = sih.StockItemID
                left join specialdeals sd on si.StockItemID = sd.StockItemID and sd.EndDate >= CURRENT_DATE() and sd.StartDate <= CURRENT_DATE()
                where (sg.StockGroupID = ? or 0 = ?)
                    and (RecommendedRetailPrice between ? and ? or 0 = ?)
                    and ((StockItemName like concat('%',?,'%') or MarketingComments like concat('%',?,'%')) or 0 = ?)
                    and QuantityOnHand > 0
                group by si.StockItemID
                limit ?, ?");

        $stmt->bind_param('iiiiissiii', $category, $categoryEnabled, $minPrice, $maxPrice, $priceEnabled, $query, $query, $queryEnabled, $startAt, $productsPerPage);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $products[] = $row;
        }

        return $products;
    }

    public function getTotalPageCount($category, $query, $minPrice, $maxPrice, $categoryEnabled, $queryEnabled, $priceEnabled)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
                select count(distinct si.StockItemID) as pageCount
                from stockitems si 
                left join stockitemstockgroups sg on si.StockItemID = sg.StockItemID 
                where (StockGroupID = ? or 0 = ?)
                    and (RecommendedRetailPrice between ? and ? or 0 = ?)
                    and ((StockItemName like concat('%',?,'%') or MarketingComments like concat('%',?,'%')) or 0 = ?)");
        $stmt->bind_param('iiiiissi', $category, $categoryEnabled, $minPrice, $maxPrice, $priceEnabled, $query, $query, $queryEnabled);
        $stmt->execute();

        $result = $stmt->get_result();
        $product = $result->fetch_all(MYSQLI_ASSOC);

        return $product[0]['pageCount'];
    }

    public function getCategories() {
        $categories = [];

        $stmt = Database::$Connections['MySQL']->prepare("
                select StockGroupID, StockGroupName 
                from stockgroups");
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }

        return $categories;
    }
}