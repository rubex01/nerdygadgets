<?php

namespace App\Controllers\Products;

use Framework\Database\Database;
use Framework\Auth\Auth;

trait LastViewedTrait
{
    public function getLastViewed(): array
    {
        $lastViewedProducts = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                SELECT si.StockItemID, Video, QuantityOnHand, StockItemName, description, Brand, Size, RecommendedRetailPrice, TypicalWeightPerUnit, MarketingComments, CustomFields, Tags, ColorName, Video, ImagePath, SpecialDealID, DealDescription, DiscountPercentage, DiscountAmount FROM userstockiteminterest ui
                left join stockitems si on ui.StockItemID = si.StockItemID
                left join stockitemholdings sh on sh.StockItemID = ui.StockItemID
                left join specialdeals sd on sd.StockItemID = ui.StockItemID and sd.EndDate >= CURRENT_DATE() and sd.StartDate <= CURRENT_DATE()
                left join colors co on co.ColorID = si.ColorID
                left join stockitemimages sim on sim.StockItemId = ui.StockItemId
                WHERE user_id = ?
                group by ui.StockItemID
                order by max(interest_id) desc
                limit 6
            "
        );
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $lastViewedProducts[] = $row;
        }
        return $lastViewedProducts;
    }
}