<?php

namespace App\Controllers\ShoppingCart;

use Framework\Pages\Pages;
use Framework\Auth\Auth;
use Framework\Database\Database;

class ShoppingCartController
{
    use GetShoppingCartItemsTrait;
    use UpdateShoppingCartItemsTrait;
    use \App\Controllers\NotificationTrait;

    public function getShoppingCart()
    {
        if (Auth::check()) {
            $shoppingCartItems = $this->getShoppingCartItems(true);
        }
        else {
            $shoppingCartItems = $this->getShoppingCartItems(false);
        }

        return Pages::view('default', 'shoppingcart', [
            'sass' => ['shoppingcart.css'],
            'js' => ['shoppingcartupdate.js'],
            'products' => $shoppingCartItems
        ]);

        return Pages::view('default', 'shoppingcart', []);
    }

    public function toFavorites($productId)
    {
        $shoppingCartItems = $this->updateShoppingCart($productId, 0, '-');

        $stmt = Database::$Connections['MySQL']->prepare("insert into favorites (user_id, StockItemID) values (?, ?)");
        $stmt->bind_param('ii', Auth::$id, $productId);
        $stmt->execute();

        $this->addNotification('Succes', 'Product is verplaatst naar uw verlanglijstje', 'success');
        header('location:/shopping-cart');
    }

    public function deleteFromShoppingCart($productId)
    {
        $shoppingCartItems = $this->updateShoppingCart($productId, 0, '-');

        $this->addNotification('Succes', 'Product is verwijderd uit de winkelwagen', 'success');
        header('location:/shopping-cart');
    }
}