<?php

namespace App\Controllers\ShoppingCart;

use Framework\Auth\Auth;
use Framework\Request\Request;
use Framework\Database\Database;

class ApiController
{
    use GetShoppingCartItemsTrait;
    use UpdateShoppingCartItemsTrait;

    public function updateToShoppingCart(Request $request)
    {
        $this->updateShoppingCart($request->input('StockItemID'), $request->input('amount'), $request->input('method'));
    }

    public function getShoppingCart(Request $request)
    {
        if (Auth::check()) {
            $shoppingCartItems = $this->getShoppingCartItems(true, false);
        }
        else {
            $shoppingCartItems = $this->getShoppingCartItems(false);
        }

        echo json_encode($shoppingCartItems);
    }
}