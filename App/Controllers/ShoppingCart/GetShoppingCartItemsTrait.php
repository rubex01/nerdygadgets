<?php

namespace App\Controllers\ShoppingCart;

use Framework\Auth\Auth;
use Framework\Database\Database;

trait GetShoppingCartItemsTrait
{
    public function getShoppingCartItems(bool $loggedIn, $collisionCheck = true) : array
    {
        $shoppingCartItems = [];
        if ($loggedIn) {
            $stmt = Database::$Connections['MySQL']->prepare("
                    SELECT shopping_id, si.StockItemID, avg(stars) stars, DiscountAmount, DiscountPercentage, RecommendedRetailPrice, ImagePath, StockItemName, amount, QuantityOnHand
                    FROM shoppingcart sc
                    left join stockitems si on sc.StockItemID = si.StockItemID
                    left join stockitemimages sim on sc.StockItemId = sim.StockItemId 
                    left join reviews r on r.StockItemID = sc.StockItemID
                    left join stockitemholdings sih on sih.StockItemID = sc.StockItemID
                    left join specialdeals sd on sd.StockItemID = sc.StockItemID and EndDate >= CURRENT_DATE and StartDate <= CURRENT_DATE
                    WHERE sc.user_id = ?
                    group by si.StockItemID
                ");
            $stmt->bind_param('i', Auth::$id);
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                if ($row['DiscountAmount'] != null && $row['DiscountAmount'] != 0 || $row['DiscountPercentage'] != null && $row['DiscountPercentage'] != 0) {
                    if ($row['DiscountAmount'] != null && $row['DiscountAmount'] != 0) {
                        $price = $row['RecommendedRetailPrice'] - $row['DiscountAmount'];
                    }
                    else {
                        $price = $row['RecommendedRetailPrice'] * (100-$row['DiscountPercentage']) / 100;
                    }
                    $shoppingCartItems[] = [
                        'shopping_id' => $row['shopping_id'],
                        'StockItemID' => $row['StockItemID'],
                        'stars' => $row['stars'],
                        'RecommendedRetailPrice'  => round($price, 2),
                        'ImagePath'  => $row['ImagePath'],
                        'StockItemName' => $row['StockItemName'],
                        'amount' => $row['amount'],
                        'QuantityOnHand' => $row['QuantityOnHand'],
                        'DiscountAmount' => $row['DiscountAmount'],
                        'DiscountPercentage' => $row['DiscountPercentage']
                    ];
                }
                else {
                    $shoppingCartItems[] = $row;
                }
            }
            if ($collisionCheck) {
                $this->CheckShoppingCartCollision($shoppingCartItems);
            }
            return $shoppingCartItems;
        }

        $shoppingCartArray = (isset($_COOKIE['shoppingcart'])) ? json_decode($_COOKIE['shoppingcart'], true) : [];

        if (count($shoppingCartArray) > 0) {
            $inQuery = '';
            $params = [];
            $types = '';

            foreach ($shoppingCartArray as $productId => $amount) {
                $inQuery .= ' ?,';
                $types .= 'i';
                $params[] = $productId;
            }

            $inQuery = substr($inQuery, 0, -1);

            $stmt = Database::$Connections['MySQL']->prepare("
                        SELECT si.StockItemID, avg(stars) stars, RecommendedRetailPrice, DiscountAmount, DiscountPercentage, ImagePath, StockItemName FROM stockitems si
                        left join stockitemimages sim on sim.StockItemId = si.StockItemId 
                        left join reviews r on r.StockItemID = si.StockItemID
                        left join specialdeals sd on sd.StockItemID = si.StockItemID and EndDate >= CURRENT_DATE and StartDate <= CURRENT_DATE
                        WHERE si.StockItemID in ($inQuery)
                        group by si.StockItemID
                    ");
            $stmt->bind_param($types, ...$params);
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                if ($row['DiscountAmount'] != null && $row['DiscountAmount'] != 0 || $row['DiscountPercentage'] != null && $row['DiscountPercentage'] != 0) {
                    if ($row['DiscountAmount'] != null && $row['DiscountAmount'] != 0) {
                        $price = $row['RecommendedRetailPrice'] - $row['DiscountAmount'];
                    } else {
                        $price = $row['RecommendedRetailPrice'] * (100 - $row['DiscountPercentage']) / 100;
                    }
                }
                else {
                    $price = $row['RecommendedRetailPrice'];
                }
                $shoppingCartItems[] = [
                    'StockItemID' => $row['StockItemID'],
                    'stars' => $row['stars'],
                    'RecommendedRetailPrice'  => round($price, 2),
                    'DiscountAmount' => $row['DiscountAmount'],
                    'DiscountPercentage' => $row['DiscountPercentage'],
                    'ImagePath' => $row['ImagePath'],
                    'StockItemName' => $row['StockItemName'],
                    'amount' => $shoppingCartArray[$row['StockItemID']]
                ];
            }
        }

        return $shoppingCartItems;
    }

    public function CheckShoppingCartCollision($shoppingCartItems)
    {
        $productQuantityCollision = [];
        foreach ($shoppingCartItems as $shoppingCartItem) {
            if ($shoppingCartItem['amount'] > $shoppingCartItem['QuantityOnHand']) {
                $productQuantityCollision[] = $shoppingCartItem;
            }
        }
        if (count($productQuantityCollision) >= 1) {
            foreach ($productQuantityCollision as $collisionItem) {
                if ($collisionItem['QuantityOnHand'] === 0) {
                    $stmt = Database::$Connections['MySQL']->prepare("delete from shoppingcart where shopping_id = ?");
                    $stmt->bind_param('i', $collisionItem['shopping_id']);
                    $stmt->execute();
                    $this->addNotification('We hebben niet genoeg voorraad', 'U heeft ' . $collisionItem['amount'] . ' ' . $collisionItem['StockItemName']. ' in uw winkelwagen. Dit product is helaas uitverkocht en we hebben het uit uw winkelwagentje verwijderd', 'error');
                }
                else {
                    $stmt = Database::$Connections['MySQL']->prepare("update shoppingcart set amount = ? where shopping_id = ?");
                    $stmt->bind_param('ii', $collisionItem['QuantityOnHand'], $collisionItem['shopping_id']);
                    $stmt->execute();
                    $this->addNotification('We hebben niet genoeg voorraad', 'U heeft ' . $collisionItem['amount'] . ' ' . $collisionItem['StockItemName']. ' in uw winkelwagen. We hebben hier niet genoeg voorraad voor en het aantal is aangepast.', 'error');
                }
            }
            header('location:/shopping-cart');
            exit();
        }
    }
}