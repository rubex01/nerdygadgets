<?php

namespace App\Controllers\ShoppingCart;

use Framework\Auth\Auth;
use Framework\Database\Database;

trait UpdateShoppingCartItemsTrait
{
    public function updateShoppingCart(int $stockItemID, $amount, $method, $forcedLogin = false)
    {
        $productId = $stockItemID;
        $totalAmount = is_int($amount);
        $method = $method;
        $amount = ($totalAmount === true) ? $amount : 1;

        if (Auth::check() || $forcedLogin === true) {
            $stmt = Database::$Connections['MySQL']->prepare(
                "select amount from shoppingcart where user_id = ? and StockItemID = ?"
            );
            $stmt->bind_param('ii', Auth::$id, $productId);
            $stmt->execute();
            $result = $stmt->get_result();

            if ($method = '+') {
                $newAmount = ($totalAmount) ? $amount : $result->fetch_all(MYSQLI_ASSOC)[0]['amount'] + $amount;
            }
            else {
                $newAmount = ($totalAmount) ? $amount : $result->fetch_all(MYSQLI_ASSOC)[0]['amount'] - $amount;
            }

            if ($newAmount === 0) {
                $stmt = Database::$Connections['MySQL']->prepare("delete from shoppingcart where user_id = ? and StockItemID = ?");
                $stmt->bind_param('ii', Auth::$id, $productId);
            }
            elseif ($result->num_rows > 0) {
                $stmt = Database::$Connections['MySQL']->prepare("update shoppingcart set amount = ? where user_id = ? and StockItemID = ?");
                $stmt->bind_param('iii', $newAmount, Auth::$id, $productId);
            }
            else {
                $stmt = Database::$Connections['MySQL']->prepare("insert into shoppingcart (user_id, StockItemID, amount) values (?, ?, ?)");
                $stmt->bind_param('iii', Auth::$id, $productId, $newAmount);
            }

            $stmt->execute();
            echo json_encode(['success' => true]);
            return true;
        }

        $shoppingcart = (isset($_COOKIE['shoppingcart'])) ? json_decode($_COOKIE['shoppingcart'], true) : [];

        if ($totalAmount === true) unset($shoppingcart[$productId]);

        $oldValue = isset($shoppingcart[$productId]) ? $shoppingcart[$productId] : 0;

        if ($method = '+') {
            $newAmount = ($totalAmount) ? $amount : $oldValue + $amount;
        }
        else {
            $newAmount = ($totalAmount) ? $amount : $oldValue - $amount;
        }

        if ($newAmount === 0) {
            unset($shoppingcart[$productId]);
        }
        else {
            $shoppingcart[$productId] = $newAmount;
        }

        $jsonShoppingCart = json_encode($shoppingcart);

        setcookie('shoppingcart', $jsonShoppingCart, time() + 3600, '/');
        echo json_encode(['success' => true]);
        return true;
    }
}