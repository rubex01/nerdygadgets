<?php

namespace App\Controllers;

trait RecaptchaTrait
{
    public function recaptchaValidation($input, $location)
    {
        $secretKey = getenv('RECAPTCHA_PRIVATE_TOKEN');

        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) . '&response=' . urlencode($input);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);

        if (!$responseKeys["success"]) {
            $this->addNotification('Invoer is niet geldig', 'reCAPTCHA ongeldig', 'error');
            header('location:'. $location);
            exit();
        }
    }
}