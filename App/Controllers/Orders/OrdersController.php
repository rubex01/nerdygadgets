<?php

namespace App\Controllers\Orders;

use Framework\Pages\Pages;
use Framework\Auth\Auth;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Framework\Database\Database;

class OrdersController
{
    use \App\Controllers\NotificationTrait;

    public function getOrders()
    {
        $user = Auth::$user;

        $orders = $this->getUserOrders($user['user_id']);

        return Pages::view('default', 'orders', [
            'sass' => ['orders.css'],
            'user' => $user,
            'orders' => $orders
        ]);
    }
    public function getUserOrders($userId) {
        $orders = [];

        $stmt = Database::$Connections['MySQL']->prepare("
                select canceled, payment_method, payment_link, cp.checkout_product_id, editable, si.StockItemID, StockItemName, ImagePath, MarketingComments, cp.amount, c.checkout_id, c.checkout_time, c.expected_delivery_time, isdelivered, address, house_number, zipcode, domicile, province province, is_payed_for
                from checkout c
                left join checkout_products cp on cp.checkout_id = c.checkout_id
                left join stockitems si on si.StockItemID = cp.StockItemID
                left join stockitemimages sii on sii.StockItemID = cp.StockItemID
                where c.user_id = ?
                group by checkout_product_id
                order by canceled, is_payed_for, editable desc, checkout_time desc");
        $stmt->bind_param('i', $userId);
        $stmt->execute();
        $result = $stmt->get_result();

        while ($row = $result->fetch_assoc()) {
            $orders[$row['checkout_id']][] = $row;
        }

        return $orders;
    }

    public function cancelOrder(Request $request, $orderId)
    {
        $stmt = Database::$Connections['MySQL']->prepare("update checkout set canceled = 1 where checkout_id = ?");
        $stmt->bind_param('i', $orderId);
        $stmt->execute();

        $this->addNotification('Annulering bevestigd', 'Uw bestelling is geannuleerd', 'success');
        header('location:/orders');
    }
}