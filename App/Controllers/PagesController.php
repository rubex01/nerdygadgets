<?php

namespace App\Controllers;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Auth\Auth;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Mailgun\Mailgun;
use Mailgun\Message\MessageBuilder;

class PagesController
{
    use \App\Controllers\Products\LastViewedTrait;
    use NotificationTrait;
    use RecaptchaTrait;

    public function getHomepage()
    {
        $lastViewed = Auth::check() ? $this->getLastViewed() : null;

        $specialDeals = [];
        $stmt = Database::$Connections['MySQL']->prepare("select sp.StockItemId, DealDescription, EndDate, DiscountAmount, DiscountPercentage, StockItemName, ImagePath from specialdeals sp left join stockitems st on st.StockItemID = sp.StockItemID left join stockitemimages si on si.StockItemID = sp.StockItemID where EndDate >= CURDATE() order by EndDate DESC limit 2");
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $specialDeals[] = $row;
        }

        $categories = [];
        $stmt = Database::$Connections['MySQL']->prepare("select StockGroupID, StockGroupName, ImagePath from stockgroups limit 7");
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $categories[] = $row;
        }

        return Pages::view('default', 'home', [
            'sass' => ['home.css'],
            'specialDeal' => $specialDeals,
            'categories' => $categories,
            'lastViewed' => $lastViewed
        ]);
    }

    public function getAgreement()
    {
        return Pages::view('default', 'static/agreement', []);
    }

    public function getPrivacyStatement()
    {
        return Pages::view('default', 'static/privacystatement', []);
    }

    public function getCovid19()
    {
        return Pages::view('default', 'static/covid-19', []);
    }

    public function getFAQ()
    {
        return Pages::view('default', 'static/faq', []);
    }

    public function getContact()
    {
        $user = Auth::check() ? Auth::$user : NULL;
        return Pages::view('default', 'static/contact',  ['user' => $user, 'sass' => ['contact.css']]);
    }

    public function postContact(Request $request){
        $validation = new Validation(
            $request->body,
            [
                'name' => ['required'],
                'email' => ['required', 'email', 'string'],
                'subject' => ['required'],
                'message' => ['required', 'string'],
                'g-recaptcha-response' => ['required']
            ],
            [
                'name.required' => 'Naam is verplicht',
                'email.email' => 'Gebruik een geldig email adres',
                'email.required' => 'Email is verplicht',
                'subject.required' => 'Onderwerp is verplicht',
                'message.required' => 'Bericht is verplicht',
                'g-recaptcha-response.required' => 'reCAPTCHA is verplicht'
            ]);

        if (count($validation->validationErrors) > 0) {
            foreach ($validation->validationErrors as $validationError) {
                $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
            }
            header('location:/contact');
            exit();
        }

        $this->recaptchaValidation($request->input('g-recaptcha-response'), '/contact');

        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');
        $stmt = Database::$Connections['MySQL']->prepare("insert into contact (email, name, subject, text) values (?, ?, ?, ?)");
        $stmt->bind_param('ssss', $name, $email, $subject, $message);
        $stmt->execute();

        $this->sendEmailContactForm($request->body);

        $this->addNotification('Succes', 'Succesvol verstuurd', 'success');
        header('location:/contact');
    }

    public function sendEmailContactForm($contactFormInfo)
    {
        $stmt = Database::$Connections['MySQL']->prepare(
            "SELECT email from contactemails"
        );
        $stmt->execute();
        $result = $stmt->get_result();
        $allRecipients = [];
        while($row = $result->fetch_assoc()) {
            $allRecipients[] = $row['email'];
        }

        if (count($allRecipients) > 0) {
            $emailContent = file_get_contents(__DIR__ . '/../../Views/Templating/Pages/mails/contact-form.page');
            $emailContent = sprintf($emailContent, $contactFormInfo['email'], $contactFormInfo['name'], $contactFormInfo['subject'], $contactFormInfo['message'], $contactFormInfo['email']);

            $builder = new MessageBuilder();
            $builder->setFromAddress(getenv('MG_FROM'));
            $builder->setSubject('Contact formulier');
            $builder->setTextBody('
                Nieuwe reactie op NerdyGadgets. 
                
                email: '.$contactFormInfo['email'].'
                naam: '.$contactFormInfo['name'].'
                onderwerp: '.$contactFormInfo['subject'].'

                Bericht:'.
                $contactFormInfo['message']
            );
            $builder->setHtmlBody($emailContent);

            foreach ($allRecipients as $recipient) {
                $builder->addToRecipient($recipient);
            }

            $mg = Mailgun::create(getenv('MG_API_KEY'));
            $mg->messages()->send(getenv('MG_DOMAIN'), $builder->getMessage());
        }
    }
}