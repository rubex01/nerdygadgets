<?php

namespace App\Controllers\Checkout;

use Framework\Auth\Auth;
use Framework\Database\Database;
use Framework\Request\Request;
use Framework\Validation\Validation;

class PaymentController
{
    use \App\Controllers\NotificationTrait;
    use \App\Controllers\ShoppingCart\GetShoppingCartItemsTrait;

    private $supportedBanks = ['ABN AMRO', 'ASN Bank', 'bunq', 'ING', 'Knab', 'Moneyou', 'Rabobank', 'RegioBank', 'SNS', 'Scenska Handelsbanken', 'Triodos Bank', 'Van Lanschot'];

    private $paymentError = false;

    public function paymentRequest(Request $request)
    {
        $orderData = json_decode($request->input('orderData'));

        $this->validatePaymentData($orderData);

        if ($this->paymentError !== false) {
            $this->abortPayment();
        }

        $currentTime = date('Y-m-d h:i:s', time());
        $expectedDeliveryTime = $this->getExpectedDeliveryTime($orderData);
        $locationData = $this->getLocationData($orderData);
        $true = true;
        $false = false;
        $paymentMethod = $this->getPaymentMethod($orderData);
        $paymentLink = md5(uniqid(Auth::$id . rand(), true));
        $orderId = md5(uniqid(Auth::$id . rand(), true));

        $stmt = Database::$Connections['MySQL']->prepare("
                insert into checkout 
                (user_id, checkout_time, expected_delivery_time, isdelivered, address, house_number, zipcode, domicile, province, editable, is_payed_for, payment_method, payment_link, order_id)
                values
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            ");
        $stmt->bind_param('ississsssiisss',
                          Auth::$id,
                          $currentTime,
                          $expectedDeliveryTime,
                          $false,
                          $locationData['address'],
                          $locationData['house_number'],
                          $locationData['zipcode'],
                          $locationData['domicile'],
                          $locationData['province'],
                          $true,
                          $false,
                          $paymentMethod,
                          $paymentLink,
                          $orderId
        );
        $stmt->execute();

        $insertId = Database::$Connections['MySQL']->insert_id;
        $this->makeCheckoutProductRows($insertId);

        $this->emptyShoppingCart();

        if ($orderData->paymentMethod === 'ideal') {
            header('location:/payment/'.$paymentLink);
            exit();
        }
        header('location:/checkout/'.$orderId);
    }

    public function makeCheckoutProductRows(int $checkoutId)
    {
        $shoppingCartItems = $this->getShoppingCartItems(true);
        $currentPrices = $this->getCurrentPrices();

        $values = '';
        $types = '';
        $params = [];

        foreach ($shoppingCartItems as $key => $shoppingCartItem) {
            $values .= '(?, ?, ?, ?)';
            $types .= 'iiis';
            $params[] = $shoppingCartItem['StockItemID'];
            $params[] = $checkoutId;
            $params[] = $shoppingCartItem['amount'];
            foreach ($currentPrices as $currentPrice) {
                if ($currentPrice['StockItemID'] === $shoppingCartItem['StockItemID']) {
                    $params[] = $currentPrice['RecommendedRetailPrice'] * $shoppingCartItem['amount'];
                }
            }
            $values .= ($key !== (count($shoppingCartItems)-1)) ? ',' : '';
        }

        $stmt = Database::$Connections['MySQL']->prepare("
                insert into checkout_products 
                (StockItemID, checkout_id, amount, price)
                values
                $values
            ");
        $stmt->bind_param($types, ...$params);
        $stmt->execute();
    }

    public function getCurrentPrices()
    {
        $productPrices = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                    select cp.StockItemID, RecommendedRetailPrice, DiscountAmount, DiscountPercentage from shoppingcart cp
                    left join stockitems si on cp.StockItemID = si.StockItemID
                    left join specialdeals sd on sd.StockItemID = cp.StockItemID and EndDate >= CURRENT_DATE and StartDate <= CURRENT_DATE
                    where cp.user_id = ?
                ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            if (
                $row['DiscountAmount'] != null && $row['DiscountAmount'] != 0 ||
                $row['DiscountPercentage'] != null && $row['DiscountPercentage'] != 0
            ) {
                if ($row['DiscountAmount'] != null && $row['DiscountAmount'] != 0) {
                    $productPrices[] = [
                        'StockItemID' => $row['StockItemID'],
                        'RecommendedRetailPrice' => $row['RecommendedRetailPrice'] - $row['DiscountAmount']
                    ];
                }
                else {
                    $productPrices[] = [
                        'StockItemID' => $row['StockItemID'],
                        'RecommendedRetailPrice' => $row['RecommendedRetailPrice'] * (100-$row['DiscountPercentage']) / 100
                    ];
                }
            }
            else {
                $productPrices[] = $row;
            }
        }
        return $productPrices;
    }

    public function emptyShoppingCart()
    {
        $stmt = Database::$Connections['MySQL']->prepare("delete from shoppingcart where user_id = ?");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
    }

    public function getExpectedDeliveryTime(object $data)
    {
        if ($data->paymentMethod === 'ideal') {
            return null;
        }
        $deliveryTime = [];
        $stmt = Database::$Connections['MySQL']->prepare("
                    SELECT max(deliverytime) deliverytime
                    FROM shoppingcart sc
                    left join stockitems si on sc.StockItemID = si.StockItemID
                    WHERE sc.user_id = ?
                ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $deliveryTime[] = $row;
        }

        if (date("H") >= 23) {
            return date("Y-m-d", time() + (86400 * ($deliveryTime[0]['deliverytime'] + 1)));
        }
        return date("Y-m-d", time() + (86400 * $deliveryTime[0]['deliverytime']));
    }

    public function getPaymentMethod(object $data)
    {
        if ($data->paymentMethod === 'ideal') {
            return json_encode([
                'method' => 'ideal',
                'bank' => $data->idealBank
                               ]);
        }
        return json_encode([
            'method' => 'payAfter'
                           ]);
    }

    public function getLocationData(object $data)
    {
        if ($data->deliveryAddressMethod === 'custom') {
            return [
                'address' => $data->deliveryAddressInfo->address,
                'house_number' => $data->deliveryAddressInfo->house_number,
                'zipcode' => $data->deliveryAddressInfo->zipcode,
                'domicile' => $data->deliveryAddressInfo->domicile,
                'province' => $data->deliveryAddressInfo->province
            ];
        }
        return [
            'address' => Auth::$user['address'],
            'house_number' => Auth::$user['house_number'],
            'zipcode' => Auth::$user['zipcode'],
            'domicile' => Auth::$user['domicile'],
            'province' => Auth::$user['province']
        ];
    }

    public function validatePaymentData(object $data)
    {
        if ($data->paymentMethod === 'ideal') {
            if (!in_array($data->idealBank, $this->supportedBanks)) {
                $this->paymentError = true;
                $this->addNotification('Ideal ondersteunt de gekozen bank niet', 'De bank die u gekozen heeft '.$data->idealBank.' wordt niet door ideal ondersteund', 'error');
            }
        }
        if ($data->deliveryAddressMethod === 'custom') {
            $validation = new Validation(
                [
                    'address' => $data->deliveryAddressInfo->address,
                    'house_number' => $data->deliveryAddressInfo->house_number,
                    'zipcode' => $data->deliveryAddressInfo->zipcode,
                    'domicile' => $data->deliveryAddressInfo->domicile,
                    'province' => $data->deliveryAddressInfo->province,
                ],
                [
                    'address' => ['required', 'string'],
                    'house_number' => ['required', 'string'],
                    'zipcode' => ['required', 'string'],
                    'domicile' => ['required', 'string'],
                    'province' => ['required', 'string']
                ],
                [
                    'address.required' => 'Adres is verplicht',
                    'house_number.required' => 'Huisnummer is verplicht',
                    'zipcode.required' => 'Postcode is verplicht',
                    'domicile.required' => 'Woonplaats is verplicht',
                    'province.required' => 'Provincie is verplicht'
                ]
            );
            if (count($validation->validationErrors) > 0) {
                foreach ($validation->validationErrors as $validationError) {
                    $this->addNotification('Invoer is niet geldig', $validationError['message'], 'error');
                }
                $this->paymentError = true;
            }
        }
    }

    public function abortPayment()
    {
        header('location:/checkout');
        exit();
    }
}