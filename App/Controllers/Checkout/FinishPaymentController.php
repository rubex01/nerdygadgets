<?php

namespace App\Controllers\Checkout;

use Framework\Auth\Auth;
use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Request\Request;
use Mailgun\Mailgun;

class FinishPaymentController
{
    use \App\Controllers\NotificationTrait;
    use UpdateStockQuantityTrait;

    public function pendingPayment(Request $request, $paymentLink)
    {
        $price = $this->orderInformation('price', $paymentLink);

        return Pages::view('payment', 'payment', [
            'leftToPay' => $price,
            'callback' => '/endpoint/payment/'.$paymentLink
        ]);
    }

    public function finishPayment(Request $request, $paymentLink)
    {
        echo 'test';

        $stmt = Database::$Connections['MySQL']->prepare("select payment_method, expected_delivery_time, order_id, email, first_name from checkout c left join users u on u.user_id = c.user_id where payment_link = ?");
        $stmt->bind_param('s', $paymentLink);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $paymentMethod = json_decode($row['payment_method']);
            $deliveryTime = $row['expected_delivery_time'];
            $orderId = $row['order_id'];
            $email = $row['email'];
            $name = $row['first_name'];
        }

        $expectedDeliveryTime = $deliveryTime;
        $true = true;
        $null = '';

        if ($paymentMethod->method === 'ideal') {
            $expectedDeliveryTime = $this->getExpectedDeliveryTime($paymentLink);
            $rollbackNeeded = false;

            Database::$Connections['MySQL']->autocommit(false);

            $this->updateStockItemQuantity('payment_link', $paymentLink) ? null : $rollbackNeeded = true;

            $stmt = Database::$Connections['MySQL']->prepare("update checkout set is_payed_for = ?, expected_delivery_time = ?, payment_link = ? where payment_link = ?");
            $stmt->bind_param('isss', $true, $expectedDeliveryTime, $null, $paymentLink);
            $stmt->execute()  ? null : $rollbackNeeded = true;

            if ($rollbackNeeded) {
                Database::$Connections['MySQL']->rollback();
                Database::$Connections['MySQL']->autocommit(true);
                header('/endpoint/payment/'.$paymentLink);
                exit();
            }

            Database::$Connections['MySQL']->commit();
            Database::$Connections['MySQL']->autocommit(true);

            $this->sendPaymentConfirmationMail($expectedDeliveryTime, $name, $orderId, $email);

            header('location:/checkout/'.$orderId);
        }
        else {
            $stmt = Database::$Connections['MySQL']->prepare("update checkout set is_payed_for = ?, expected_delivery_time = ?, payment_link = ? where payment_link = ?");
            $stmt->bind_param('issi', $true, $expectedDeliveryTime, $null, $paymentLink);
            $stmt->execute();

            $this->sendPaymentConfirmationMail($expectedDeliveryTime, $name, $orderId, $email);

            $this->addNotification('Betaling gelukt', 'Er is succesvol betaald.', 'success');
            header('location:/');
        }
    }

    public function sendPaymentConfirmationMail($expectedDeliveryTime, $name, $orderId, $email)
    {
        $emailContent = file_get_contents(__DIR__ . '/../../../Views/Templating/Pages/mails/payment-success.page');
        $date = date('d-m-Y', strtotime($expectedDeliveryTime));
        $emailContent = sprintf($emailContent, $name, $date, $orderId);

        $mg = Mailgun::create(getenv('MG_API_KEY'));
        $mg->messages()->send(getenv('MG_DOMAIN'), [
            'from'    => getenv('MG_FROM'),
            'to'      => $email,
            'subject' => 'Je betaling is voltooid',
            'text'    => 'Je hebt succesvol betaald voor je bestelling.',
            'html'    => $emailContent
        ]);
    }

    public function getExpectedDeliveryTime(string $paymentLink)
    {
        $maxDaysBeforeDelivery = $this->orderInformation('deliveryTime', $paymentLink);

        if (date("H") >= 23) {
            return date("Y-m-d", time() + (86400 * ($maxDaysBeforeDelivery + 1)));
        }
        return date("Y-m-d", time() + (86400 * $maxDaysBeforeDelivery));
    }

    public function orderInformation(string $infoType, string $paymentLink)
    {
        $stmt = Database::$Connections['MySQL']->prepare("
                select SUM(cp.price) price, max(deliverytime) deliverytime from checkout co
                left join checkout_products cp on cp.checkout_id = co.checkout_id
                left join stockitems si on si.StockItemID = cp.StockItemID
                where payment_link = ?
            ");
        $stmt->bind_param('s', $paymentLink);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $price = $row['price'];
            $deliveryTimeDays = $row['deliverytime'];
        }
        if ($infoType === 'price') {
            return round($price, 2);
        }
        return $deliveryTimeDays;
    }
}