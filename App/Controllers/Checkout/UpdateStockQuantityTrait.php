<?php

namespace App\Controllers\Checkout;

use Framework\Database\Database;

trait UpdateStockQuantityTrait
{
    public function updateStockItemQuantity($column, $identifier)
    {
        $orderRows = [];
        $stmt = Database::$Connections['MySQL']->prepare("select amount, StockItemID from checkout c left join checkout_products cp on cp.checkout_id = c.checkout_id where $column = ?");
        $stmt->bind_param('s', $identifier);
        $executeSelect = $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $orderRows[] = $row;
        }
        foreach ($orderRows as $orderRow) {
            $stmt = Database::$Connections['MySQL']->prepare("update stockitemholdings set QuantityOnHand = (QuantityOnHand - ?) where StockItemID = ?");
            $stmt->bind_param('ii', $orderRow['amount'], $orderRow['StockItemID']);
            $executeUpdate = $stmt->execute();
        }

        if ($executeUpdate === true && $executeSelect === true) {
            return true;
        }
        return false;
    }
}