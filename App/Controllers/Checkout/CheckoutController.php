<?php

namespace App\Controllers\Checkout;

use Framework\Database\Database;
use Framework\Pages\Pages;
use Framework\Auth\Auth;
use Framework\Request\Request;
use Framework\Validation\Validation;
use Mailgun\Mailgun;

class CheckoutController
{
    use \App\Controllers\ShoppingCart\GetShoppingCartItemsTrait;
    use \App\Controllers\NotificationTrait;
    use UpdateStockQuantityTrait;

    public function getCheckout()
    {
        $user = Auth::$user;

        if (date("H") >= 23) {
            $deliveryDate = date("Y-m-d", time() + (86400 * $this->getDeliveryDate()));
        }
        else {
            $deliveryDate = date("Y-m-d", time() + (86400 * $this->getDeliveryDate() + 1));
        }

        return Pages::view('default', 'checkout', [
            'sass' => ['checkout.css'],
            'js' => ['checkout/pagination.js', 'checkout/checkout.js'],
            'user' => $user,
            'addressFullySetup' => $this->checkAddressInfo($user),
            'deliveryTime' => $deliveryDate,
            'shoppingCartItems' => $this->getShoppingCartItems(true)
        ]);
    }

    public function paymentRequest(Request $request)
    {
        return Pages::view('default', 'payment', [
            'sass' => ['checkout.css'],
            'orderData' => $request->input('orderData')
        ]);
    }

    public function checkAddressInfo(array $user)
    {
        if (
            $user['address'] !== null && $user['address'] !== '' &&
            $user['house_number'] !== null && $user['house_number'] !== '' &&
            $user['zipcode'] !== null && $user['zipcode'] !== '' &&
            $user['domicile'] !== null && $user['domicile'] !== '' &&
            $user['province'] !== null && $user['province'] !== ''
        ) {
            return true;
        }
        return false;
    }

    public function getDeliveryDate()
    {
        $stmt = Database::$Connections['MySQL']->prepare("
                    SELECT max(deliverytime) deliverytime
                    FROM shoppingcart sc
                    left join stockitems si on sc.StockItemID = si.StockItemID
                    WHERE sc.user_id = ?
                ");
        $stmt->bind_param('i', Auth::$id);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $deliveryTime[] = $row;
        }
        return $deliveryTime[0]['deliverytime'];
    }

    public function finishedCheckout(Request $request, $orderId)
    {
        Database::$Connections['MySQL']->autocommit(false);
        $rollbackNeeded = false;

        $orderRows = [];
        $stmt = Database::$Connections['MySQL']->prepare("select amount, sum(price) price, payment_method, payment_link, StockItemID, email, expected_delivery_time, first_name from checkout c left join users u on u.user_id = c.user_id left join checkout_products cp on cp.checkout_id = c.checkout_id where order_id = ?");
        $stmt->bind_param('s', $orderId);
        $stmt->execute() ? null : $rollbackNeeded = true;
        $result = $stmt->get_result();
        while ($row = $result->fetch_assoc()) {
            $orderRows[] = $row;
            $paymentMethod = json_decode($row['payment_method']);
            $email = $row['email'];
            $paymentLink = $row['payment_link'];
            $name = $row['first_name'];
            $price = $row['price'];
        }

        if ($paymentMethod->method === 'payAfter') {
            $this->updateStockItemQuantity('order_id', $orderId) ? null : $rollbackNeeded = true;

            $emailContent = file_get_contents(__DIR__ . '/../../../Views/Templating/Pages/mails/payafter-request.page');
            $emailContent = sprintf($emailContent, $name, $price, $paymentLink, $price);

            $mg = Mailgun::create(getenv('MG_API_KEY'));
            $mg->messages()->send(getenv('MG_DOMAIN'), [
                'from'    => getenv('MG_FROM'),
                'to'      => $email,
                'subject' => 'Achteraf betalen',
                'text'    => 'http://localhost/payment/'.$paymentLink,
                'html'    => $emailContent
            ]);
        }

        if ($rollbackNeeded) {
            Database::$Connections['MySQL']->rollback();
            Database::$Connections['MySQL']->autocommit(true);
            header('/checkout/'.$orderId);
            exit();
        }
        Database::$Connections['MySQL']->commit();
        Database::$Connections['MySQL']->autocommit(true);

        return Pages::view('default', 'checkout-finished', [
            'deliveryDate' => $orderRows[0]['expected_delivery_time'],
            'sass' => ['checkoutfinished.css']
        ]);
    }
}