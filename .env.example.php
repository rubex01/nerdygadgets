<?php
$variables = [
    //Application info
    'APP_NAME' => 'NerdyGadgets',
    'CACHING' => false,
    'ENVIRONMENT' => 'development',
    'DEBUGGING' => true,
    'BASEPATH' => '/',
    'AUTOCOMPILE' => true,

    //Default MySQL database credentials
    'DB_HOST' => 'localhost',
    'DB_USERNAME' => 'root',
    'DB_PASSWORD' => '',
    'DB_NAME' => 'nerdygadgets_webshop',

    //New user with right autorisation
    'DB_WEBSHOP_USER_PASSWORD' => 'A password for your new database user',

    //Cookies
    'DOMAIN' => null,
    'SECURE' => false,
    'HTTP_ONLY' => true,

    //mailgun
    'MG_API_KEY' => '',
    'MG_DOMAIN' => 'sandbox87ff18f043634a8c8ce4996ea3edeae5.mailgun.org',
    'MG_FROM' => 'no.reply@nerdygadgets.nl',

    //Temperature update token
    'TEMP_TOKEN' => 'Choose a random token',

    //Recaptcha token
    'RECAPTCHA_PRIVATE_TOKEN' => 'key',
    'RECAPTCHA_PUBLIC_TOKEN' => 'key',

    // Admin panel salt
    'ADMIN_SALT' => 'put your secret salt here'
];