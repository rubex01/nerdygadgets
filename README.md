# NerdyGadgets webshop
#### This repository contains a webshop called: "NerdyGadgets". 

#### Requirements
* apache webserver (for example: XAMPP)
* support for PHP 7.3 and higher
* get_browser php extension enabled
* MySQL database

#### Setup for developers
1. Clone this repository
1. Host the application with the webserver
1. Create an empty database with the name: `nerdygadgets_webshop`
1. Run the setup command inside the folder where you cloned the repository: `php ./Framework/Commands/Run.php`
    1. Choose (Y)es
    1. Choose (N)o
    1. The app will now be able to load in your browser at http://localhost

#### Documentation of packages used
* View the documentation of the custom framework for this project [here](https://github.com/rubex01/framework-rubex/wiki)
