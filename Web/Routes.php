<?php

use Framework\Routing\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here you can register new routes for your app. See the docs to get
| started. Now create something nice!
|
*/

/*
|------------------------------
| Static pages
|------------------------------
*/
Route::get('/', [App\Controllers\PagesController::class, 'getHomepage']);

Route::get('/check', function () {
    echo "online";
});

Route::get('/agreement', [App\Controllers\PagesController::class, 'getAgreement']);

Route::get('/privacy-statement', [App\Controllers\PagesController::class, 'getPrivacyStatement']);

Route::get('/covid-19', [App\Controllers\PagesController::class, 'getCovid19']);

Route::get('/faq', [App\Controllers\PagesController::class, 'getFAQ']);

Route::get('/contact', [App\Controllers\PagesController::class, 'getContact']);


/*
|------------------------------
| Product pages
|------------------------------
*/
Route::get('/shopping-cart', [App\Controllers\ShoppingCart\ShoppingCartController::class, 'getShoppingCart']);

Route::get('/products', [App\Controllers\Products\ProductsController::class, 'getProductspage']);

Route::get('/product/(.*)', [App\Controllers\Products\ProductController::class, 'getProduct']);

/*
|------------------------------
| Authentication pages
|------------------------------
*/
Route::get('/login', [App\Controllers\Auth\LoginController::class, 'getLogin'])->middleware(['noAuth']);

Route::get('/registration', [App\Controllers\Auth\RegistrationController::class, 'getAccountRegistration'])->middleware(['noAuth']);

Route::get('/my-account', [App\Controllers\Auth\AccountController::class, 'getAccount'])->middleware(['auth']);

Route::get('/password-reset', [App\Controllers\Auth\PasswordResetController::class, 'getPasswordResetReq'])->middleware(['noAuth']);

Route::get('/password-reset/(.*)', [App\Controllers\Auth\PasswordResetController::class, 'getResetPassword'])->middleware(['noAuth']);

/*
|------------------------------
| Order pages
|------------------------------
*/
Route::get('/orders', [App\Controllers\Orders\OrdersController::class, 'getOrders'])->middleware(['auth']);

/*
|------------------------------
| Favorites routes
|------------------------------
*/
Route::get('/favorites', [App\Controllers\Favorites\FavoritesController::class, 'getFavorites'])->middleware(['auth']);

/*
|------------------------------
| Checkout routes
|------------------------------
*/
Route::get('/checkout', [App\Controllers\Checkout\CheckoutController::class, 'getCheckout'])->middleware(['auth', 'shoppingCartFull']);

Route::get('/payment/(.*)', [App\Controllers\Checkout\FinishPaymentController::class, 'pendingPayment']);

Route::get('/checkout/(.*)', [App\Controllers\Checkout\CheckoutController::class, 'finishedCheckout']);

/*
|------------------------------
| Retour Routes
|------------------------------
*/
Route::get('/retour', [App\Controllers\Retour\RetourController::class, 'getRetour'])->middleware(['auth']);

Route::get('/retour/(.*)', [App\Controllers\Retour\RetourController::class, 'getRetourProduct'])->middleware(['auth']);


/*
|------------------------------
| Endpoint routes
|------------------------------
*/
Route::group(
    [
        'prefix' => '/endpoint',
        'routes' => [
            Route::post('/product/(.*)/review', [App\Controllers\Products\ProductController::class, 'postReview'])->middleware(['auth']),
            Route::post('/registration', [App\Controllers\Auth\RegistrationController::class, 'registerAction']),
            Route::get('/logout', [App\Controllers\Auth\LogoutController::class, 'logout'])->middleware(['auth']),
            Route::post('/login', [App\Controllers\Auth\LoginController::class, 'loginAction']),
            Route::post('/contact', [App\Controllers\PagesController::class, 'postContact']),
            Route::post('/favorites/(.*)/delete', [App\Controllers\Favorites\FavoritesController::class, 'deleteFavorite'])->middleware(['auth']),
            Route::post('/my-account/accountdata', [App\Controllers\Auth\AccountController::class, 'updateAccountData'])->middleware(['auth']),
            Route::post('/my-account/addressdata', [App\Controllers\Auth\AccountController::class, 'updateAddressData'])->middleware(['auth']),
            Route::post('/delete-account', [App\Controllers\Auth\AccountController::class, 'deleteAccount'])->middleware(['auth']),
            Route::post('/my-account/password', [App\Controllers\Auth\AccountController::class, 'updatePassword'])->middleware(['auth']),
            Route::get('/shoppingcart/tofav/(.*)', [App\Controllers\ShoppingCart\ShoppingCartController::class, 'toFavorites'])->middleware(['auth']),
            Route::get('/shoppingcart/(.*)', [App\Controllers\ShoppingCart\ShoppingCartController::class, 'deleteFromShoppingCart']),
            Route::post('/checkout', [App\Controllers\Checkout\PaymentController::class, 'paymentRequest'])->middleware(['auth', 'shoppingCartFull']),
            Route::get('/payment/(.*)', [App\Controllers\Checkout\FinishPaymentController::class, 'finishPayment']),
            Route::post('/retour/(.*)', [App\Controllers\Retour\RetourController::class, 'retourProduct']),
            Route::post('/password-reset', [App\Controllers\Auth\PasswordResetController::class, 'requestPasswordReset'])->middleware(['noAuth']),
            Route::post('/password-reset/set', [App\Controllers\Auth\PasswordResetController::class, 'passwordResetAction'])->middleware(['noAuth']),
            Route::post('/logout/(.*)', [App\Controllers\Auth\AccountController::class, 'signedOutDevice'])->middleware(['auth']),
            Route::get('/uptodate/(.*)', [App\Controllers\Products\UptoDateController::class, 'setUptodateTrigger'])->middleware(['auth']),
            Route::get('/order/(.*)', [App\Controllers\Orders\OrdersController::class, 'cancelOrder'])->middleware(['auth']),
        ]
    ]
);

/*
|------------------------------
| API routes
|------------------------------
*/
Route::group(
    [
        'prefix' => '/api',
        'routes' => [
            Route::get('/favorites', [App\Controllers\Favorites\ApiController::class, 'getFavorites'])->middleware(['apiAuth']),
            Route::put('/favorites', [App\Controllers\Favorites\ApiController::class, 'updateFavorites'])->middleware(['apiAuth']),
            Route::put('/shoppingcart', [App\Controllers\ShoppingCart\ApiController::class, 'updateToShoppingCart']),
            Route::get('/shoppingcart', [App\Controllers\ShoppingCart\ApiController::class, 'getShoppingCart']),
            Route::get('/productinfo/(.*)', [App\Controllers\Products\ApiController::class, 'getProductInfo']),
            Route::post('/register-temp', [App\Controllers\TemperatureController::class, 'updateTemperature'])->disableCSRF(),
            Route::get('/get-temperatures', [App\Controllers\TemperatureController::class, 'getLiveTemperatures'])->disableCSRF(),
        ]
    ]
);



/*
|------------------------------
| Admin panel
|------------------------------
*/
Route::get('/admin', [App\Controllers\Admin\OverViewController::class, 'getOverview'])->middleware(['adminAuth']);

Route::group(
    [
        'prefix' => '/admin',
        'routes' => [
            /*
            |------------------------------
            | Stock pages
            |------------------------------
            */
            Route::get('/stock', [App\Controllers\Admin\StockController::class, 'getstockAction'])->middleware(['adminAuth']),
            Route::get('/stock/edit/(.*)', [App\Controllers\Admin\StockController::class, 'getStockEdit'])->middleware(['adminAuth']),

            /*
            |------------------------------
            | Product pages
            |------------------------------
            */
            Route::get('/products', [App\Controllers\Admin\Products\ProductController::class, 'getProducts'])->middleware(['adminAuth']),
            Route::get('/products/add', [App\Controllers\Admin\Products\AddProductController::class, 'getAddProduct'])->middleware(['adminAuth']),
            Route::get('/products/edit/(.*)', [App\Controllers\Admin\Products\EditProductController::class, 'getEditProduct'])->middleware(['adminAuth']),

            /*
            |------------------------------
            | Login routes
            |------------------------------
            */
            Route::get('/login', [App\Controllers\Admin\Auth\LoginController::class, 'getLogin']),

            /*
           |------------------------------
           | Sales pages
           |------------------------------
           */
            Route::get('/sales', [App\Controllers\Admin\Sales\SalesController::class, 'getSales'])->middleware(['adminAuth']),
            Route::get('/sales/edit/(.*)', [App\Controllers\Admin\Sales\SalesController::class, 'getEdit'])->middleware(['adminAuth']),
            Route::get('/sales/add', [App\Controllers\Admin\Sales\SalesController::class, 'getAdd'])->middleware(['adminAuth']),

            /*
            |------------------------------
            | Setting pages
            |------------------------------
            */
            Route::get('/settings', [App\Controllers\Admin\SettingsController::class, 'getSettings'])->middleware(['adminAuth']),

            /*
            |------------------------------
            | Users pages
            |------------------------------
            */
            Route::get('/users', [App\Controllers\Admin\Users\UserController::class, 'getUsers'])->middleware(['adminAuth']),
            Route::get('/users/add', [App\Controllers\Admin\Users\AddUserController::class, 'getAddUser'])->middleware(['adminAuth']),
            Route::get('/users/edit/(.*)', [App\Controllers\Admin\Users\EditUserController::class, 'getEditUser'])->middleware(['adminAuth']),
            Route::get('/setup/(.*)', [App\Controllers\Admin\Auth\SetupController::class, 'getSetup'])

        ]
    ]
);

/*
|------------------------------
| Admin endpoint routes
|------------------------------
*/
Route::group(
    [
        'prefix' => '/admin/endpoint',
        'routes' => [
            Route::post('/login', [App\Controllers\Admin\Auth\LoginController::class, 'loginAction']),
            Route::get('/products/edit/image/(.*)/(.*)', [App\Controllers\Admin\Products\ImageController::class, 'removeImage'])->middleware(['adminAuth']),
            Route::post('/products/edit/image/(.*)', [App\Controllers\Admin\Products\ImageController::class, 'addImage'])->middleware(['adminAuth']),
            Route::get('/products/edit/tag/(.*)/(.*)', [App\Controllers\Admin\Products\CategoryController::class, 'removeTag'])->middleware(['adminAuth']),
            Route::post('/products/edit/tag/(.*)', [App\Controllers\Admin\Products\CategoryController::class, 'addCategory'])->middleware(['adminAuth']),
            Route::post('/products/edit/(.*)', [App\Controllers\Admin\Products\EditInformationController::class, 'updateInformation'])->middleware(['adminAuth']),
            Route::post('/products/add', [App\Controllers\Admin\Products\AddProductController::class, 'storeProduct'])->middleware(['adminAuth']),
            Route::get('/products/delete/(.*)', [App\Controllers\Admin\Products\DeleteController::class, 'deleteProduct'])->middleware(['adminAuth']),
            Route::post('/stock/(.*)', [App\Controllers\Admin\StockController::class, 'updateStock'])->middleware(['adminAuth']),
            Route::get('/sales/delete/(.*)', [App\Controllers\Admin\Sales\SalesController::class, 'getDelete'])->middleware(['adminAuth']),
            Route::post('/sales/edit/(.*)', [App\Controllers\Admin\Sales\SalesController::class, 'updateSale'])->middleware(['adminAuth']),
            Route::post('/sales/add', [App\Controllers\Admin\Sales\SalesController::class, 'storeSale'])->middleware(['adminAuth']),
            Route::get('/logout', [App\Controllers\Admin\Auth\LogoutController::class, 'AdminLogout'])->middleware(['adminAuth']),
            Route::post('/settings/password', [App\Controllers\Admin\SettingsController::class, 'updatePassword'])->middleware(['adminAuth']),
            Route::post('/settings/logout/(.*)', [App\Controllers\Admin\SettingsController::class, 'adminSignOutDevice'])->middleware(['adminAuth']),
            Route::post('/settings/theme', [App\Controllers\Admin\SettingsController::class, 'updateTheme'])->middleware(['adminAuth']),
            Route::get('/settings/theme-reset', [App\Controllers\Admin\SettingsController::class, 'resetTheme'])->middleware(['adminAuth']),
            Route::get('/users/delete/(.*)', [App\Controllers\Admin\Users\DeleteUserController::class, 'deleteUser'])->middleware(['adminAuth']),
            Route::post('/users/edit/(.*)', [App\Controllers\Admin\Users\EditUserController::class, 'editUser'])->middleware(['adminAuth']),
            Route::post('/users/add', [App\Controllers\Admin\Users\AddUserController::class, 'addUser'])->middleware(['adminAuth']),
            Route::post('/setup/(.*)', [App\Controllers\Admin\Auth\SetupController::class, 'setupAction']),
            Route::post('/settings/profile-picture', [App\Controllers\Admin\SettingsController::class, 'updatePhoto'])->middleware(['adminAuth']),
            Route::post('/settings/contact-email', [App\Controllers\Admin\SettingsController::class, 'storeEmail'])->middleware(['adminAuth']),
            Route::get('/settings/delete-email/(.*)', [App\Controllers\Admin\SettingsController::class, 'deleteContactEmail'])->middleware(['adminAuth']),
        ]
    ]
);