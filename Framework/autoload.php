<?php

spl_autoload_register(function ($className) {

    $extension = '.php';

    $fullPath =  $className . $extension;

    $fullPath = str_replace('\\', '/', $fullPath);

    if (file_exists(__DIR__ . '/../' . $fullPath)) {
        include_once __DIR__ . '/../' . $fullPath;

        if (method_exists($className, 'init')) {
            $className::init();
        }
    }

});
