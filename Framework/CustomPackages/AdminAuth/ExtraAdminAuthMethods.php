<?php

namespace Framework\CustomPackages\AdminAuth;

trait ExtraAdminAuthMethods
{
    public static function initialAuthCheck()
    {
        if (!isset($_COOKIE['adminToken'])) return false;

        $deviceFingerPrint = self::$deviceFingerPrint;

        $getBrowserJson = json_encode($deviceFingerPrint['getBrowser']);

        $stmt = self::$database->prepare('SELECT * FROM admin_user_sessions WHERE token = ? AND user_agent = ? AND get_browser_info = ? AND ip_address = ?');
        $stmt->bind_param("ssss", $_COOKIE['adminToken'], $deviceFingerPrint['userAgent'], $getBrowserJson, $deviceFingerPrint['ipAddress']);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $session = $result->fetch_assoc();
            if ($session['valid_until'] > date('Y-m-d H:i:s')) {

                $userData = self::getUserData('PersonID', $session['PersonID']);
                unset($userData['HashedPassword']);

                self::$user = $userData;
                self::$id = $userData['PersonID'];

                return true;
            }
        }
        self::logout();
        return false;
    }

    private static function getUserData(string $column, string $selectItem)
    {
        $stmt = self::$database->prepare("SELECT * FROM people WHERE $column = ? AND ValidFrom <= now() and ValidTo >= now() AND IsPermittedToLogon = 1");
        $stmt->bind_param("s", $selectItem);
        $stmt->execute();
        $userData = $stmt->get_result()->fetch_assoc();
        self::$passwordHash = $userData['HashedPassword'];
        return $userData;
    }

    private static function generateLogin(array $userData)
    {
        $deviceFingerPrint = self::$deviceFingerPrint;

        $token = bin2hex(random_bytes(64));
        $getBrowserJson = json_encode($deviceFingerPrint['getBrowser']);
        $timestamp = date('Y-m-d H:i:s', strtotime("+3 month", strtotime(date("Y/m/d"))));

        $stmt = self::$database->prepare('INSERT INTO admin_user_sessions (PersonID, token, valid_until, user_agent, get_browser_info, browser, operating_system, device_type, ip_address) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
        $stmt->bind_param("sssssssss",
                          $userData['PersonID'],
                          $token,
                          $timestamp,
                          $deviceFingerPrint['userAgent'],
                          $getBrowserJson,
                          $deviceFingerPrint['getBrowser']['browser'],
                          $deviceFingerPrint['getBrowser']['platform'],
                          $deviceFingerPrint['getBrowser']['device_type'],
                          $deviceFingerPrint['ipAddress']
        );
        $stmt->execute();

        setcookie('adminToken', $token, time() + (86400 * 99), '/', getenv('DOMAIN'), getenv('SECURE'), getenv('HTTP_ONLY'));

        return true;
    }

    /**
     * Get device fingerprint
     *
     * @return array
     */
    private function getDeviceFingerprint()
    {
        return [
            'userAgent' => $_SERVER['HTTP_USER_AGENT'],
            'getBrowser' => get_browser('empty', true),
            'ipAddress' => $_SERVER['REMOTE_ADDR']
        ];
    }

}